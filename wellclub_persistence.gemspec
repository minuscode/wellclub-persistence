# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'wellclub_persistence/version'

Gem::Specification.new do |spec|
  spec.name          = "wellclub_persistence"
  spec.version       = WellclubPersistence::VERSION
  spec.authors       = ["minuscode"]
  spec.email         = ["contact@minuscode.com"]
  spec.description   = 'Wellclub Persistence Gem'
  spec.summary       = 'Wellclub Persistence is an ActiveRecord-based persistence layer for Wellclub Rails applications'
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'rails', '~> 4.1.4'
  #spec.add_dependency 'strong_parameters'
  spec.add_dependency 'attr_encrypted'
  spec.add_dependency 'aws-sdk', '< 2.0'
  spec.add_dependency 'paperclip', '~> 4.2'
  spec.add_dependency 'mini_magick', "~> 4.0.0.rc"
  spec.add_dependency 'sendgrid'

  # authentication and authorization dependencies
  spec.add_dependency 'devise'
  spec.add_dependency 'devise-async'
  spec.add_dependency 'omniauth'
  spec.add_dependency 'omniauth-facebook'
  spec.add_dependency 'sidekiq'
  spec.add_dependency 'cancancan', "~> 1.9"

  spec.add_dependency 'aasm'
  spec.add_dependency 'friendly_id', '~> 5.0.0'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
