module NotificationDispatcherDesktop
  #SUGGESTIONS NOTIFICATIONS
  def self.send_notification_suggestion(suggestion_user, prompt)
    if suggestion_user.supporter.present?

      supporter_element = Supporter.where(:friend_id => suggestion_user.supporter_id, :user_id => suggestion_user.user_id)
      user_to_notify = suggestion_user.supporter

      if prompt.eql? "shared_suggestion"
        user_to_notify = suggestion_user.user
      end

      if supporter_element.present?
        notification = Notification.get_notification(prompt, SuggestionUser)
        notification.create_user_notification(user_to_notify.id,suggestion_user.id, :desktop)
      end
    end
  end

  def self.send_notification_suggestion_to_supporters(suggestion_user, prompt)
    if suggestion_user.supporter.present? #only to the one who shared
       #suggestion_user.user.accepted_supporters.joins("inner join users on users.id = supporters.friend_id").each do |supporter|
      notification = Notification.get_notification(prompt, SuggestionUser)
      notification.create_user_notification(suggestion_user.supporter.id, suggestion_user.id, :desktop)
       #end
    end
  end

  #NEEDS DESKTOP?
  def self.send_notification_suggestion_prompts(suggestion_user)
  end

  def self.send_notification_comment(suggestion_user_comment)

    #excludes the user that commented
    exclude_ids = [suggestion_user_comment.user.id]
    user_to_notify = suggestion_user_comment.suggestion_user.user.id
    unless exclude_ids.include?(user_to_notify)
  #    Notification.create_notification(user_to_notify, suggestion_user_comment.id, SuggestionUserComment)
        prompt = "suggestion_comment"
        notification = Notification.get_notification(prompt, SuggestionUserComment)
        notification.create_user_notification(user_to_notify,suggestion_user_comment.id, :desktop)
    end
  end

  def self.send_notification_comment_to_supporters(suggestion_user_comment)
    exclude_ids = [suggestion_user_comment.user.id]
    user_to_notify = suggestion_user_comment.suggestion_user.user.id
    exclude_ids << user_to_notify

    if suggestion_user_comment.suggestion_user.supporter.present? and !exclude_ids.include?(suggestion_user_comment.suggestion_user.supporter.id)
      exclude_ids << suggestion_user_comment.suggestion_user.supporter.id
      prompt = "suggestion_comment_supporter"
      notification = Notification.get_notification(prompt, SuggestionUserComment)
      notification.create_user_notification(suggestion_user_comment.suggestion_user.supporter.id,suggestion_user_comment.id, :desktop)
    end

    #to 3rd party user
    third_party = SuggestionUserComment.where.not(user_id: exclude_ids).where(suggestion_user_id: suggestion_user_comment.suggestion_user.id)
    third_party.each do |comment|
       if !exclude_ids.include?(comment.user.id)
         exclude_ids << comment.user.id
         prompt = "suggestion_comment_supporter_3rd"
         notification = Notification.get_notification(prompt, SuggestionUserComment)
         notification.create_user_notification(comment.user.id,suggestion_user_comment.id, :desktop)
       end
    end
  end


#GOALS NOTIFICATIONS
  def self.send_notification_goal(goal_user, prompt)
    if goal_user.user.present?
      notification = Notification.get_notification(prompt, GoalUser)
      notification.create_user_notification(goal_user.user.id,goal_user.id, :desktop)
    end
  end

  def self.send_notification_goal_to_supporters(goal_user, prompt)
     goal_user.user.accepted_supporters.joins(:user).where(where).each do |supporter|
        notification = Notification.get_notification(prompt, GoalUser)
        notification.create_user_notification(goal_user.user.id,goal_user.id, :desktop)
     end
  end


#SUPPORTERS NOTIFICATIONS
  def self.send_notification_supporters(supporter, prompt, extra)
    user_to_notify = supporter.user.id
    if prompt.eql? "invite_supporter_existing"
      user_to_notify = supporter.friend.id
    end
    notification = Notification.get_notification(prompt, Supporter)
    notification.create_user_notification(user_to_notify, supporter.id, :desktop)
  #  Notific ation.create_notification(supporter.user.id, support.id, Support)
  end

  def self.send_notification_members(supporter, prompt, extra)
    user_to_notify = supporter.friend.id

    if prompt.eql? 'invite_member_existing'
      user_to_notify = supporter.user.id
    end

    notification = Notification.get_notification(prompt, Supporter)
    notification.create_user_notification(user_to_notify, supporter.id, :desktop)
  end

end

