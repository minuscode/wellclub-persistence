module NotificationDispatcherEmail

#SUGGESTIONS NOTIFICATIONS
  def self.send_notification_suggestion(suggestion_user, prompt)
    if suggestion_user.supporter.present?

      supporter_element = Supporter.where(:friend_id => suggestion_user.supporter_id, :user_id => suggestion_user.user_id)
      user_to_notify = suggestion_user.supporter
      send = false
      case prompt
        when 'started_suggestion_supporter'
          send = suggestion_user.supporter.notif_suggestion_accepted_shared
        when "retried_suggestion_supporter"
          send = suggestion_user.supporter.notif_suggestion_retried_shared
        when "rejected_suggestion_supporter"
          send = suggestion_user.supporter.notif_suggestion_rejected_shared
        when "shared_suggestion"
          send = suggestion_user.user.notif_suggestion_new
          user_to_notify = suggestion_user.user
      end

      if supporter_element.present? and send
        SendEmailWorker.perform_async(suggestion_user.id, prompt, supporter_element.first.id)
        notification = Notification.get_notification(prompt, SuggestionUser)
        notification.create_user_notification(user_to_notify.id,suggestion_user.id, :email)
      end
    end
  end

  def self.send_notification_suggestion_to_supporters(suggestion_user, prompt)
    if suggestion_user.supporter.present? #only to the one who shared
       if prompt.eql? "prompt_f"
          send = suggestion_user.supporter.notif_suggestion_completed_shared
       elsif prompt.eql? "prompt_1" and prompt.eql? "prompt_2" and prompt.eql? "prompt_3"
          send = suggestion_user.supporter.notif_suggestion_expired_shared
       end
       supporter_element = Supporter.where(:friend_id => suggestion_user.supporter_id, :user_id => suggestion_user.user_id)

       if send and supporter_element.present?
          SendEmailWorker.perform_async(suggestion_user.id, "#{prompt}_supporter", supporter_element.first.id)
          notification = Notification.get_notification(prompt, SuggestionUser)
          notification.create_user_notification(suggestion_user.supporter.id, suggestion_user.id, :email)
       end
    end
  end


  def self.send_notification_suggestion_prompts(suggestion_user)
      if suggestion_user.user.notif_suggestion_expired
        case suggestion_user.prompt_status.to_sym
          when :prompt_0
            SendEmailWorker.perform_async(suggestion_user.id, suggestion_user.prompt_status)
          when :prompt_1
            SendEmailWorker.perform_async(suggestion_user.id, suggestion_user.prompt_status)
          when :prompt_2
            SendEmailWorker.perform_async(suggestion_user.id, suggestion_user.prompt_status)
          when :prompt_3
            SendEmailWorker.perform_async(suggestion_user.id, suggestion_user.prompt_status)
            return
        end
      end
  end

  def self.send_notification_comment(suggestion_user_comment)

    #excludes the user that commented
    exclude_ids = [suggestion_user_comment.user.id]
    user_to_notify = suggestion_user_comment.suggestion_user.user.id
    unless exclude_ids.include?(user_to_notify)
      exclude_ids << user_to_notify
  #    Notification.create_notification(user_to_notify, suggestion_user_comment.id, SuggestionUserComment)
      if suggestion_user_comment.suggestion_user.user.notif_suggestion_comment
         prompt = "suggestion_comment"
         SendEmailWorker.perform_async(suggestion_user_comment.id, prompt)
         notification = Notification.get_notification(prompt, SuggestionUserComment)
         notification.create_user_notification(user_to_notify,suggestion_user_comment.id, :email)
      end
    end

  end

  def self.send_notification_comment_to_supporters(suggestion_user_comment)
    exclude_ids = [suggestion_user_comment.user.id]
    user_to_notify = suggestion_user_comment.suggestion_user.user.id
    exclude_ids << user_to_notify

    if suggestion_user_comment.suggestion_user.supporter.present? and !exclude_ids.include?(suggestion_user_comment.suggestion_user.supporter.id)
      exclude_ids << suggestion_user_comment.suggestion_user.supporter.id
      if suggestion_user_comment.suggestion_user.supporter.notif_suggestion_comment
         SendEmailWorker.perform_async(suggestion_user_comment.id, "suggestion_comment_supporter")
         prompt = "suggestion_comment_supporter"
         notification = Notification.get_notification(prompt, SuggestionUserComment)
         notification.create_user_notification(suggestion_user_comment.suggestion_user.supporter.id,suggestion_user_comment.id, :email)

      end
    end

    #to 3rd party user
    third_party = SuggestionUserComment.where.not(user_id: exclude_ids).where(suggestion_user_id: suggestion_user_comment.suggestion_user.id)
    third_party.each do |comment|
    #   Notification.create_notification(comment.user.id, suggestion_user_comment.id, SuggestionUserComment)
       if !exclude_ids.include?(comment.user.id) and comment.user.notif_suggestion_comment
          exclude_ids << comment.user.id
          SendEmailWorker.perform_async(suggestion_user_comment.id, "suggestion_comment_supporter_3rd", comment.user.id)
          prompt = "suggestion_comment_supporter_3rd"
          notification = Notification.get_notification(prompt, SuggestionUserComment)
          notification.create_user_notification(comment.user.id,suggestion_user_comment.id, :email)
       end
    end
  end


#GOALS NOTIFICATIONS
  def self.send_notification_goal(goal_user, prompt)
    if goal_user.user.present?

      send = false
      case prompt
        when 'prompt_goal_expired'
          send = goal_user.user.notif_expired_goal
      end

      if goal_user.present? and send
        SendEmailWorker.perform_async(goal_user.id, prompt)
        notification = Notification.get_notification(prompt, GoalUser)
        notification.create_user_notification(goal_user.user.id,goal_user.id, :email)
      end
    end
  end



  def self.send_notification_goal_to_supporters(goal_user, prompt)
     where = ''
     where = 'notif_achieved_goal_supporter = true' if prompt.eql? "prompt_goal_achieved"
     where = 'notif_expired_goal_supporter = true' if prompt.eql? "prompt_goal_expired"

     goal_user.user.accepted_supporters.joins(:user).where(where).each do |supporter|
        SendEmailWorker.perform_async(goal_user.id, "#{prompt}_supporter", supporter.id)
        notification = Notification.get_notification(prompt, GoalUser)
        notification.create_user_notification(goal_user.user.id,goal_user.id, :email)
     end
  end


#SUPPORTERS NOTIFICATIONS
  def self.send_notification_supporters(supporter, prompt, extra)
    send = false
    user_to_notify = supporter.user.id
    case prompt
      when 'supporter_accepted'
        send = supporter.friend.notif_invitation_accepted
      when 'invite_supporter_existing'
        send = supporter.friend.notif_invitation
        user_to_notify = supporter.friend.id
    end

    if send
      SendEmailWorker.perform_async(supporter.id, prompt, extra )
      notification = Notification.get_notification(prompt, Supporter)
      notification.create_user_notification(user_to_notify, supporter.id, :email)
    end
  #  Notification.create_notification(supporter.user.id, support.id, Support)
  end

  def self.send_notification_members(supporter, prompt, extra)
    send = false
    user_to_notify = supporter.friend.id

    case prompt
      when 'member_accepted'
        send = supporter.friend.notif_invitation_accepted
      when 'invite_member_existing'
        send = supporter.user.notif_invitation_member
        user_to_notify = supporter.user.id
    end

    if send
      SendEmailWorker.perform_async(supporter.id, prompt, extra )
      notification = Notification.get_notification(prompt, Supporter)
      notification.create_user_notification(user_to_notify, supporter.id, :email)
    end
  #  Notification.create_notification(supporter.user.id, support.id, Support)
  end



end

