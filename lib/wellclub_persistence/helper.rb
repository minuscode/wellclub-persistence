module WellclubPersistence
  module Helper
    require 'net/http'
    require "resolv-replace.rb"

    def self.working_url?(url)
      begin
        url = URI.parse(url)
        #req = Net::HTTP.new(url.host, url.port, {open_timeout: 5, read_timeout: 5})
        req = Net::HTTP.start(url.host, url.port, {open_timeout: 5, read_timeout: 5})
        res = req.request_head(url.path)

        [res.code == "200", url]
      rescue Exception
        [false, url]
      end
    end

    def self.get_string_between(my_string, start_at, end_at)
        my_string = " #{my_string}"
        ini = my_string.index(start_at)
        return my_string if ini == 0
        ini += start_at.length
        length = my_string.index(end_at, ini).to_i - ini
        my_string[ini,length]
    end

    def self.average_calculation_date(date_type, first_date, last_date, num) 
      diff = (last_date - first_date)
      if diff == 0
        return num/1
      end
      case date_type        
      when :day
        days = diff/1.day
        days < 1 ? num/days.ceil : num/days.to_f
      when :week
        weeks = diff/7.day
        weeks < 1 ? num/weeks.ceil : num/weeks.to_f
      when :month
        months = diff/1.month
        months < 1 ? num/months.ceil : num/months.to_f
      else
        raise ArgumentError, "Bad date_type, try 'day', 'week' or 'month'"
      end
    end
  end
end
