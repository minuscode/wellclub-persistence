#add other types of notification requires
require 'notification_dispatcher_email'
require 'notification_dispatcher_desktop'


module NotificationDispatcher
  #SUGGESTIONS NOTIFICATIONS
  def self.send_notification_suggestion(suggestion_user, prompt)
    NotificationDispatcherDesktop.send_notification_suggestion(suggestion_user, prompt)
    NotificationDispatcherEmail.send_notification_suggestion(suggestion_user, prompt)
  end

  def self.send_notification_suggestion_to_supporters(suggestion_user, prompt)
    NotificationDispatcherDesktop.send_notification_suggestion_to_supporters(suggestion_user, prompt)
    NotificationDispatcherEmail.send_notification_suggestion_to_supporters(suggestion_user, prompt)
  end

  def self.send_notification_suggestion_prompts(suggestion_user)
    NotificationDispatcherDesktop.send_notification_suggestion_prompts(suggestion_user)
    NotificationDispatcherEmail.send_notification_suggestion_prompts(suggestion_user)
  end

  def self.send_notification_comment(suggestion_user_comment)
    NotificationDispatcherDesktop.send_notification_comment(suggestion_user_comment)
    NotificationDispatcherEmail.send_notification_comment(suggestion_user_comment)
  end

  def self.send_notification_comment_to_supporters(suggestion_user_comment)
    NotificationDispatcherDesktop.send_notification_comment_to_supporters(suggestion_user_comment)
    NotificationDispatcherEmail.send_notification_comment_to_supporters(suggestion_user_comment)
  end

#GOALS NOTIFICATIONS
  def self.send_notification_goal(goal_user, prompt)
    NotificationDispatcherDesktop.send_notification_goal(goal_user, prompt)
    NotificationDispatcherEmail.send_notification_goal(goal_user, prompt)
  end

  def self.send_notification_goal_to_supporters(goal_user, prompt)
    NotificationDispatcherDesktop.send_notification_goal_to_supporters(goal_user, prompt)
    NotificationDispatcherEmail.send_notification_goal_to_supporters(goal_user, prompt)

  end

#SUPPORTERS NOTIFICATIONS
  def self.send_notification_supporters(supporter, prompt, extra = nil)
    NotificationDispatcherDesktop.send_notification_supporters(supporter, prompt, extra)
    NotificationDispatcherEmail.send_notification_supporters(supporter, prompt, extra)
  end

  def self.send_notification_members(suggestion_user, prompt, extra = nil)
    NotificationDispatcherDesktop.send_notification_members(suggestion_user, prompt, extra)
    NotificationDispatcherEmail.send_notification_members(suggestion_user, prompt, extra)
  end
end

