class LifestyleCategory
  #BEWARE -> if a value is removed the db indexes most be recalculated because whats in the db is the array index
  CATEGORIES = {:movement => 'M', :eating => 'E', :self_view => 'SV', :miscellaneous => 'MS'}
end
