class Application < Rails::Application

  SUGGESTION_PAPERCLIP_OPTS = (Rails.env.production? || Rails.env.staging? || Rails.env.demo?) ? {
    # production
    :storage => :s3,
    :s3_credentials => {
      bucket:  Rails.application.secrets.bucket,
      access_key_id:  Rails.application.secrets.access_key_id,
      secret_access_key:  Rails.application.secrets.secret_access_key
    },
    :s3_protocol => "https",
    :path => ":class/:style/:id/:filename",
    :url =>':s3_domain_url'
  } : {
    # development
    #:url => '/system/:attachment/:id/:style/:basename.:extension',

  }

 DEFAULTS  =   (Rails.env.production? || Rails.env.staging? || Rails.env.demo?) ? {
      :storage => :s3,
      :s3_credentials =>
      {
        bucket:  Rails.application.secrets.bucket,
        access_key_id:  Rails.application.secrets.access_key_id,
        secret_access_key:  Rails.application.secrets.secret_access_key
      },
      #:s3_host_name => 's3-us-east-1.amazonaws.com',
      :s3_protocol => "https",
      :path => ":class/:style/:id/:filename",
      :url =>':s3_domain_url'
  } : {

  }
end
