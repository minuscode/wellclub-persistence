ActionMailer::Base.smtp_settings = {
  :address => "smtp.sendgrid.net",
  :port => 25,
  :domain => "minuscode.com",
  :authentication => :plain,
  :user_name => Rails.application.secrets.sendgrid_user,
  :password => Rails.application.secrets.sendgrid_password
}
