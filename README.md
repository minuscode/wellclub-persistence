# WellclubPersistence

TODO: Write a gem description

## How to setup project

To setup this project please take a look at the following [page](https://bitbucket.org/minuscode/wellclub/src/71b2d4dfda8a4439dd08467edac41624accedd53/README.md?at=master).

## Installation

Add this line to your application's Gemfile:

    gem 'wellclub_persistence'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install wellclub_persistence

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request