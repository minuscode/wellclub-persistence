class UserMailer < ActionMailer::Base
  include SendGrid
  sendgrid_enable :opentrack, :clicktrack
  default from: "support@wellclub.me"
  #self.async = true
  #sendgrid_category :use_subject_lines
  #sendgrid_enable   :ganalytics, :opentrack
  #sendgrid_unique_args :key1 => "value1", :key2 => "value2"

  def welcome_message(user_id)
    # not being used
    sendgrid_category "Welcome"
    #sendgrid_unique_args :key2 => "newvalue2", :key3 => "value3"
    @user = User.find(user_id)
    @url = home_path

    mail :to => @user.email, :subject => "Welcome to WellClub - Confirm your email address"
  end

  def invite_admin(token)
    sendgrid_category "Admin Invitation"
    #sendgrid_unique_args :key2 => "newvalue2", :key3 => "value3"
    token = Token.find(token)

    if Rails.env.production?
      if ENV["MENV"].present?
        host = Rails.application.secrets.admin_url
      else
        host = Rails.application.secrets.admin_staging_url
      end
    else
      host = Rails.application.secrets.admin_url
    end

    @body_text = "Please follow the next link to start your registration."
    @body_text = "Please follow the next link and use your application credentials to enter Wellclub Backoffice." if token.user.present?

    @invite_url = new_user_registration_url(host: host , token: token.token)
    mail :to => token.email, :subject => "Welcome to WellClub Backoffice"
  end

  def feedback(user_id, message)
    sendgrid_category "Sent feedback"
    @user = User.find(user_id)
    @message = message

    mail :from => @user.email, :to => "support@wellclub.me", :subject => "Feedback from #{@user.name}"
  end

  def invite_supporter(supporter_id, message_user)
    sendgrid_category "SR: Mem - n/a supp"
    #sendgrid_unique_args :key2 => "newvalue2", :key3 => "value3"

    supporter = Supporter.find(supporter_id)
    @user = supporter.user
    @message_user = message_user

    @invite_url =  new_user_registration_url(token: supporter.token.token)
    @url = learn_more

    mail :to => supporter.friend_email, :subject => "#{@user.name} would like your support on WellClub"
  end

  def invite_supporter_existing(supporter_id, message_user)
    sendgrid_category "SR: Mem - supp"
    #sendgrid_unique_args :key2 => "newvalue2", :key3 => "value3"
    supporter = Supporter.find(supporter_id)
    @user = supporter.user
    @sup = supporter.friend
    @message_user = message_user

    unsubscribe(@sup)
    settings(@sup)

    #@invite_url =  supporter_invitation_user_url(id: supporter.id)
    @invite_url =  new_user_session_url
    @url = learn_more

    mail :to => @sup.email, :subject => "#{@user.name} would like your support on WellClub"
  end

  def supporter_accepted(supporter_id)
    sendgrid_category "SR accepted"

    supporter = Supporter.find(supporter_id)
    @user = supporter.user
    @supporter = supporter.friend

    unsubscribe(@user)
    settings(@user)

    #@invite_url =  new_user_registration_url(email: supporter_email)
    @url = home_path
    mail :to => @user.email, :subject => "#{@supporter.safe_name} is now supporting you"
  end


  def invite_member(supporter_id, message_user)
    sendgrid_category "SR: Mem - n/a Mem"
    #sendgrid_unique_args :key2 => "newvalue2", :key3 => "value3"
    supporter = Supporter.find(supporter_id)
    @user = supporter.friend
    @message_user = message_user

    @invite_url =  new_user_registration_url(token: supporter.token.token)
    @url = learn_more

    mail :to => supporter.friend_email, :subject => "#{@user.name} wants to support you on WellClub -- a new way to take small actions for your health and well-being each day."
  end

  def invite_member_existing(supporter_id, message_user)
    sendgrid_category "SR: Mem - Mem"
    #sendgrid_unique_args :key2 => "newvalue2", :key3 => "value3"
    supporter = Supporter.find(supporter_id)
    @user = supporter.user
    @sup = supporter.friend
    @message_user = message_user
    #@invite_url =  supporter_invitation_user_url(id: supporter.id)
    @invite_url =  new_user_session_url
    @url = learn_more

    unsubscribe(@user)
    settings(@user)


    mail :to => @user.email, :subject => "#{@sup.name} would like to support you"
  end

  def member_accepted(supporter_id)
    sendgrid_category "SR accepted"

    supporter = Supporter.find(supporter_id)
    @user = supporter.user
    @supporter = supporter.friend

    #@invite_url =  new_user_registration_url(email: supporter_email)
    @url = home_path
    unsubscribe(@supporter)
    settings(@supporter)


    mail :to => @supporter.email, :subject => "#{@user.safe_name} accepted your request"
  end


  def shared_suggestion(suggestion_user_id, supporter_id)

    sendgrid_category "New sugg"
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @supporter = suggestion_user.supporter
    @user = suggestion_user.user
    @suggestion = suggestion_user.suggestion
    suggestion_user = (suggestion_user.parent_suggestion_user.present? ? suggestion_user.parent_suggestion_user : suggestion_user)
    @comment = suggestion_user.suggestion_user_comments.where(:user => @supporter)
    @comment = (@comment.present? ? @comment.last : nil)
    #@invite_url =  new_user_registration_url(email: supporter_email)
    @url = home_path(@suggestion.id)
    unsubscribe(@user)
    settings(@user)

    mail :to => @user.email, :from => @supporter.email, :reply_to => "chat+#{suggestion_user.id}@wellclub.me", :subject => "#{@supporter.safe_name} sent you a suggestion"
  end


  def suggestion_comment(comment_id)
    sendgrid_category "Suggestion comment"
    @comment = SuggestionUserComment.find(comment_id)
    @supporter = @comment.user
    @user = @comment.suggestion_user.user
    #@comment = comment
    suggestion_name = "on your current suggestion"
    suggestion_name = "on your suggestion" if @comment.suggestion_user.state == 4 #shared suggestion

    unsubscribe(@user)
    settings(@user)

    #@invite_url =  new_user_registration_url(email: supporter_email)
    @url = home_path(@comment.suggestion_user.suggestion.id)
    mail :to => @user.email, :from => @supporter.email, :reply_to => "chat+#{@comment.suggestion_user.id}@wellclub.me", :subject => "#{@supporter.safe_name} commented #{suggestion_name}"
  end

  def suggestion_comment_supporter(comment_id)
    sendgrid_category "Suggestion comment"
    @comment = SuggestionUserComment.find(comment_id)
    @supporter = @comment.user
    @user = @comment.suggestion_user.supporter
    @user_suggestion = @comment.suggestion_user.user

    suggestion_name = "commented on #{@user_suggestion.safe_name}'s current suggestion"
    suggestion_name = "commented on #{@user_suggestion.safe_name}'s suggestion" if @comment.suggestion_user.state == 4 #shared suggestion

    #@comment = comment
    #@invite_url =  new_user_registration_url(email: supporter_email)
    @url = "#{root_url}friends/#{@user_suggestion.slug}?ots=#{@comment.suggestion_user.suggestion.id}"

    if @supporter.id == @user_suggestion.id
      gender_string = 'his'
      if @supporter.gender.eql? 'female'
       gender_string =  "her"
      end
      gender_string = "their" if @supporter.gender.blank?

      suggestion_name = "commented on #{gender_string} current suggestion"
      suggestion_name = "commented on their suggestion" if @comment.suggestion_user.state == 4 #shared suggestion
    end

    text = "#{@supporter.safe_name} #{suggestion_name}"
    unsubscribe(@user)
    settings(@user)

    mail :to => @user.email, :from => @supporter.email, :reply_to => "chat+#{@comment.suggestion_user.id}@wellclub.me", :subject => text
  end

  def suggestion_comment_supporter_3rd(comment_id, user_id)
    sendgrid_category "Suggestion comment"
    @comment = SuggestionUserComment.find(comment_id)
    @supporter = @comment.user
    @user = User.find(user_id)
    @user_suggestion = @comment.suggestion_user.user
    #@comment = comment
    #@invite_url =  new_user_registration_url(email: supporter_email)
    suggestion_name = "commented on #{@user_suggestion.safe_name}'s current suggestion"
    suggestion_name = "commented on #{@user_suggestion.safe_name}'s suggestion" if @comment.suggestion_user.state == 4 #shared suggestion

    @url = "#{root_url}friends/#{@user_suggestion.slug}?ots=#{@comment.suggestion_user.suggestion.id}"

    if @supporter.id == @user_suggestion.id
      gender_string = 'his'
      if @supporter.gender.eql? 'female'
       gender_string =  "her"
      end
      gender_string = "their" if @supporter.gender.blank?

      suggestion_name = "commented on #{gender_string} current suggestion"
      suggestion_name = "commented on their suggestion" if @comment.suggestion_user.state == 4 #shared suggestion

    end
    unsubscribe(@user)
    settings(@user)

    text = "#{@supporter.safe_name} #{suggestion_name}"
    mail :to => @user.email, :from => @supporter.email, :reply_to => "chat+#{@comment.suggestion_user.id}@wellclub.me", :subject => text
  end


  #prompts
  def prompt_0(suggestion_user_id)
    sendgrid_category "Sugg about to expire"
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    @user = suggestion_user.user
    #it must be suggestion detail link
    @url = home_path(@suggestion.id)
    unsubscribe(@user)
    settings(@user)

    mail :to => @user.email, :subject => "You can do it. Just a few more hours to complete #{@suggestion.name}"
  end

  def prompt_1(suggestion_user_id)
    sendgrid_category "Sugg expired"
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    @user = suggestion_user.user
    #it must be suggestion detail link
    @url = home_path(@suggestion.id)
    unsubscribe(@user)
    settings(@user)

    mail :to => @user.email, :subject => "Need a little nudge? Your suggestion expired."
  end

  def prompt_2(suggestion_user_id)
    sendgrid_category "Sugg expired + 24hrs"
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    @user = suggestion_user.user
    #it must be suggestion detail link
    @url = home_path(@suggestion.id)
    unsubscribe(@user)
    settings(@user)

    mail :to => @user.email, :subject => "Tell us what's up."
  end

  def prompt_3(suggestion_user_id)
    sendgrid_category "Sugg expired + 7days"
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    @user = suggestion_user.user
    #it must be suggestion detail link
    @url = home_path(@suggestion.id)
    unsubscribe(@user)
    settings(@user)

    mail :to => @user.email, :subject => "It's a new day."
  end

  #prompts for supporters
  def started_suggestion_supporter(suggestion_user_id, supporter_id)
    sendgrid_category "Your mem accepted your sugg"

    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    unsubscribe(@supporter)
    settings(@supporter)

    #it must be suggestion detail link
    @url = "#{root_url}friends/#{@user.slug}?ots=#{@suggestion.id}"
    mail :to => @supporter.email, :subject => "#{@user.name} started the suggestion #{@suggestion.name}."
  end

  def retried_suggestion_supporter(suggestion_user_id, supporter_id)
    sendgrid_category "Your mem retried your sugg"

    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    unsubscribe(@supporter)
    settings(@supporter)

    #it must be suggestion detail link
    @url = "#{root_url}friends/#{@user.slug}?ots=#{@suggestion.id}"
    mail :to => @supporter.email, :subject => "#{@user.name} retried the suggestion #{@suggestion.name}."
  end


  def rejected_suggestion_supporter(suggestion_user_id, supporter_id)
    sendgrid_category "Your mem rejected your sugg"

    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    unsubscribe(@supporter)
    settings(@supporter)

    #it must be suggestion detail link
    @url = "#{root_url}friends/#{@user.slug}"
    mail :to => @supporter.email, :subject => "#{@user.name} rejected the suggestion #{@suggestion.name}."
  end


  def prompt_1_supporter(suggestion_user_id, supporter_id)
    sendgrid_category "Your mem sugg about to expire"
    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    unsubscribe(@supporter)
    settings(@supporter)

    #it must be suggestion detail link
    @url = "#{root_url}friends/#{@user.slug}?ots=#{@suggestion.id}"
    mail :to => @supporter.email, :subject => "Check in with #{@user.name} b/c the suggestion expired."
  end

  def prompt_2_supporter(suggestion_user_id, supporter_id)
    sendgrid_category "Your mem sugg expired"
    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion
    #it must be suggestion detail link
    @url = "#{root_url}friends/#{@user.slug}?ots=#{@suggestion.id}"
    unsubscribe(@supporter)
    settings(@supporter)

    mail :to => @supporter.email, :subject => "#{@user.name}'s suggestion expired."
  end

  def prompt_3_supporter(suggestion_user_id, supporter_id)
    sendgrid_category "Your mem sugg expired + 24hrs"
    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion

    #it must be suggestion detail link
    @url = "#{root_url}friends/#{@user.slug}?ots=#{@suggestion.id}"
    mail :to => @supporter.email, :subject => "#{@user.name}'s suggestion expired."
  end

  def prompt_f_supporter(suggestion_user_id, supporter_id)
    sendgrid_category "Your mem sugg expired + 7days"
    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    suggestion_user = SuggestionUser.find(suggestion_user_id)
    @suggestion = suggestion_user.suggestion

    #it must be suggestion detail link
    @url = "#{root_url}friends/#{@user.slug}"

    state = (suggestion_user.state == 2 ? 'completed' : ((suggestion_user.state ==8  ? 'did not complete' : '') ))

    subject= "#{@user.name} #{state} a suggestion"
    subject= "#{@user.name} #{state} your suggestion" if suggestion_user.supporter_id == @supporter.id
    unsubscribe(@supporter)
    settings(@supporter)

    mail :to => @supporter.email, :subject => subject
  end


  def prompt_goal_expired(goal_user_id)
    sendgrid_category "Goal expired"
    @goal_user = GoalUser.find(goal_user_id)
    @goal = @goal_user.goal
    @user = @goal_user.user
    #it must be suggestion detail link
    @url = home_path
    unsubscribe(@user)
    settings(@user)

    mail :to => @user.email, :subject => "Your goal date has passed!"

  end

  def prompt_goal_expired_supporter(goal_user_id, supporter_id)
    sendgrid_category "Your mem completed goal"
    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    @goal_user = GoalUser.find(goal_user_id)
    @goal = @goal_user.goal
    @url = "#{root_url}friends/#{@user.slug}"
    unsubscribe(@supporter)
    settings(@supporter)

    mail :to => @supporter.email, :subject => "#{@user.name}'s goal date has passed!"
  end

  def prompt_goal_achieved_supporter(goal_user_id, supporter_id)
    sendgrid_category "Your mem goal expired"
    @supporter_link = Supporter.find(supporter_id)
    @supporter = @supporter_link.friend
    @user = @supporter_link.user
    @goal_user = GoalUser.find(goal_user_id)
    @goal = @goal_user.goal
    @url = "#{root_url}friends/#{@user.slug}"
    unsubscribe(@supporter)
    settings(@supporter)

    mail :to => @supporter.email, :subject => "#{@user.name}'s did it!"
  end

 private
  def home_path(suggestion_id = nil)
    extra = ''
    extra = "?ots=#{suggestion_id}" if suggestion_id.present?

    "#{root_url}home#{extra}"
  end

  def learn_more
    "#{root_url}/enter#learn-more"
  end

  def unsubscribe(user)
     @unsubscribe = "#{root_url}users/#{user.id}/unsubscribe_notifications"
  end
  def settings(user)
     @settings = "#{root_url}users/#{user.id}/settings"
  end

end
