class DeviceMailer < Devise::Mailer
  include Devise::Controllers::UrlHelpers
  include SendGrid

  def confirmation_instructions(record, token, opts={})
    sendgrid_category "Invitation"
    opts[:from] = 'support@wellclub.me'
    opts[:subject] = "Welcome to WellClub!"
    user = User.find(record)
    @supporter = user.role.eql? "supporter"
    @unsubscribe = "#{root_url}users/#{user.id}/settings"
    #opts[:subject] = "Thanks for being a WellClub Supporter.  Confirm your email address." if @supporter
    super
  end

  def reset_password_instructions(record, token, opts={})
    sendgrid_category "Invitation"
    opts[:from] = 'support@wellclub.me'
    opts[:subject] = "WellClub reset password instructions"
    super
  end

  def unlock_instructions(record, token, opts={})
    sendgrid_category "Invitation"
    opts[:from] = 'support@wellclub.me'
    super
  end

end
