class PromptWorker
  include Sidekiq::Worker

  sidekiq_options :retry => 3, :queue => :prompts

  def perform(object_id, user_name = '', prompt_id = '')
    suggestion_user = SuggestionUser.find_by_id(object_id)
    #different then complete state
    return if !suggestion_user

    if suggestion_user.state == 1
      case suggestion_user.prompt_status.to_sym
        when :prompt_0
          NotificationDispatcher.send_notification_suggestion_prompts(suggestion_user)
          suggestion_user.send_prompt_1!
          suggestion_user.save
        when :prompt_1
          NotificationDispatcher.send_notification_suggestion_prompts(suggestion_user)
          suggestion_user.send_notification_to_supporters(suggestion_user.prompt_status)
          suggestion_user.send_prompt_2!
          suggestion_user.save
        when :prompt_2
          NotificationDispatcher.send_notification_suggestion_prompts(suggestion_user)
          suggestion_user.send_notification_to_supporters(suggestion_user.prompt_status)
          suggestion_user.send_prompt_3!
          suggestion_user.save
        when :prompt_3
          NotificationDispatcher.send_notification_suggestion_prompts(suggestion_user)
          suggestion_user.send_notification_to_supporters(suggestion_user.prompt_status)
          suggestion_user.save
          return
      end
    end
  end
end