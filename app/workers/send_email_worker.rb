class SendEmailWorker
  include Sidekiq::Worker

  sidekiq_options :retry => 3, :queue => :mailer

  def perform(object_id, action, options = nil)
  	if options.nil?
  	  UserMailer.send(action, object_id).deliver
  	else
  	  UserMailer.send(action, object_id, options).deliver
  	end
  end
end