class PromptGoalWorker
  include Sidekiq::Worker

  sidekiq_options :retry => 3, :queue => :prompts

  def perform(object_id, prompt_id = '')
    goal_user = GoalUser.find_by_id(object_id)
    return if !goal_user
    p goal_user.status
    #different then complete state
    if goal_user.status == "started"
        NotificationDispatcher.send_notification_goal(goal_user, "prompt_goal_expired")
        goal_user.failed_goal
        goal_user.save
    end
    return
  end
end