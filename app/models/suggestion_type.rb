class SuggestionType < ActiveRecord::Base
	# Relationships
	has_many :suggestions, :dependent => :nullify

	enum tile_type: [ :webcontent, :actionable, :static]

end
