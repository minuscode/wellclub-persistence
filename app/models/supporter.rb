class Supporter < ActiveRecord::Base
  #include AASM
  # Relationships
  belongs_to :friend, :class_name => "User"
  belongs_to :user, :class_name => "User"
  belongs_to :token

  #validates_uniqueness_of :user_id, :scope => [:friend_id], :if => 'friend_id.present?'


  def send_member_email
    NotificationDispatcher.send_notification_supporters(self, "supporter_accepted")
  end

  def send_supporter_email
    NotificationDispatcher.send_notification_members(self, "member_accepted")
  end

  def is_accepted?
    self.state == 'accepted'
  end
=begin
  aasm :column => 'state' do
    state :invited, :initial => true
    state :accepted
    state :denied

    event :accept do
      after do
        # send email to friend saying supporter X accepted his request
       # UserMailer.supporter_accepted(self.user, self.friend).deliver
        SendEmailWorker.perform_async(self.id, "supporter_accepted")
      end
      error do |e|
      end
      transitions :from => :invited, :to => :accepted
    end

    event :deny do
      after do
        # send email to friend saying supporter X rejected his request
      end
      error do |e|
      end
      transitions :from => :invited, :to => :accepted
    end
  end
=end
end
