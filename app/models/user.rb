class User < ActiveRecord::Base
  extend FriendlyId
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/

  # Include default devise modules. Others available are:
   # :lockable, :timeoutable, :confirmable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :confirmable, :async, :omniauth_providers => [:facebook]

  attr_accessor :invited_supporters, :invited_supportees, :invited_supportee, :invited_supporter, :message
  attr_writer :current_step

  # Relationships
  has_many :identities

  has_many :payments, :dependent => :nullify

  has_many :supporters, :dependent => :destroy
  has_many :friends, :through => :supporters, :foreign_key => "user_id"
  has_many :users, :through => :supporters, :foreign_key => "friend_id", :dependent => :nullify

  has_many :user_properties,:dependent => :destroy
  has_many :properties, :through => :user_properties

  has_many :user_tags,:dependent => :destroy
  has_many :tags, :through => :user_tags

  has_many :goal_users, :dependent => :destroy
  has_many :goals, :through => :goal_users
  has_many :mood_histories,:dependent => :destroy
  has_many :weight_histories,:dependent => :destroy
  has_many :suggestion_users,:dependent => :destroy
  has_many :suggestions, :through => :suggestion_users
  has_many :playlists, :through => :suggestion_users

  has_many :user_notifications,:dependent => :destroy
  has_many :notifications, :through => :user_notifications

  has_many :tokens

  belongs_to :quote, :class_name => "TileMessage"
  belongs_to :daily_nugget, :class_name => "TileMessage"
  belongs_to :plan

  friendly_id :name, use: [:slugged, :finders]

  # Validations
  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update
  validates_presence_of :email, :name

  has_attached_file :avatar, {:styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => lambda { |avatar| avatar.instance.set_default_url} }.merge(Application::DEFAULTS)
  validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  # only for ios devices
  attr_accessor :orientation_avatar
  before_avatar_post_process :verify_rotation_avatar

  # Callbacks
  after_create :check_invites, :expire_token
  after_initialize :set_default_role
  before_create :set_trial
  before_destroy :destroy_supporters
  before_save :ensure_authentication_token, :set_start_weight, :populate_fields
  after_save :save_mood_history, :save_weight_history

  #roles
  ROLES = %w[super_admin client_admin editor writer professional member supporter]
  ROLES_ADMIN = %w[super_admin client_admin editor writer professional]
  ROLES_APP = %w[professional member supporter]
  DEFAULT_ROLE = "member"
  DEFAULT_ROLE_ADMIN = "professional"
  SUPPORTER_ROLE = "supporter"

  #gender
  GENDERS = %w[male female other]
  #moods
  MOODS = [1,2,3,4,5]
  #notifications array to know all kinds of notifications that exists
  NOTIFICATIONS = %w[notif_expired_goal notif_expired_goal_supporter notif_achieved_goal_supporter notif_invitation_accepted
    notif_invitation notif_suggestion_comment notif_suggestion_new notif_suggestion_needed notif_suggestion_expired notif_suggestion_accepted_shared
    notif_suggestion_completed_shared notif_suggestion_rejected_shared notif_suggestion_expired_shared notif_suggestion_didnt_completed_shared
    notif_suggestion_retried_shared notif_invitation_member]

  scope :admin_users, lambda { |role_admin|  where(:role_admin => User.roles_admin(role_admin))   } #lambda { |author_id| where(:author_id => author_id) }
  scope :app_users, -> { where(:role => ROLES_APP) } #lambda { |author_id| where(:author_id => author_id) }
  scope :member_users, -> { where(:role => DEFAULT_ROLE) } #lambda { |author_id| where(:author_id => author_id) }
  #scope :active, -> { where(:enabled => true) } #lambda { |author_id| where(:author_id => author_id) }


  #elastis search indexs, mappings and searches
  def self.search_ahead_analysis
    {
      index: {
        number_of_shards: 1,
        number_of_replicas: 0,
        analysis: {
          filter: {
            autocomplete_filter: {
              type: 'edge_ngram',
              min_gram: 2,
              max_gram: 20
            }
          },
          analyzer: {
            autocomplete: {
              type: 'custom',
              tokenizer: 'standard',
              filter: [
                'lowercase',
                'asciifolding',
                'autocomplete_filter'
              ]
            }
          }
        }
      }
    }
  end


  # elastic search mappings
  settings User.search_ahead_analysis do
    mappings dynamic: 'false' do
      indexes :name , :type => "string", store:"no", include_in_all: "false", index_analyzer: "autocomplete", search_analyzer: "standard"
      indexes :locked , :type => "boolean"
      indexes :slug , :type => "string", :index =>"not_analyzed"
      indexes :role , :type => "string", :index =>"not_analyzed"
    end
  end

  #after_update -> {
  #  __elasticsearch__.index_document
  #}

  def as_indexed_json(options={})
  {
    "name" => self.name,
    "locked" => self.locked,
    "slug" => self.slug,
    "role" => self.role
  }
  end

  def self.search(query, locked_users = nil, size = 0, from = 0)
    size = User.count if size == 0

    if locked_users.nil?
     User.__elasticsearch__.search(
        from: from,
        size: size,
        query: {
          match: {
            name: { query: query,
              operator: "and" }
          }
        }
    )
    else
    User.__elasticsearch__.search(
        from: from,
        size: size,
        query: {
          match: {
            name: { query: query,
              operator: "and" }
        }
        },
        filter: {
            bool: {
              should: [
                exists: { field: "role" }],
              must_not: {
                 terms: { role: ROLES_ADMIN}
              },
              should: {term: {locked: locked_users}}
           }
         }
      )

    end
  end

  #Methods
  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user

    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          name: auth.extra.raw_info.name,
          #username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        #user.skip_confirmation!
        user.save!
      end
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end

    user
  end

  def number_notifications
    number = self.user_notifications.where(:read => false, :notification_type => UserNotification.notification_types[:desktop]).count

    if number > 99
      return "+99"
    elsif number > 0
      return number.to_s
    end
    ""
  end
  def update_index(obj)
    self.__elasticsearch__.index_document
  end

  # Methods
  def verify_rotation_avatar
    if self.orientation_avatar.present?
    #  path = (Rails.env.development? ? self.avatar.path : self.avatar.url)
      image = MiniMagick::Image.new(self.avatar.queued_for_write[:original].path)
      if image.exif["Orientation"].present?
        if self.orientation_avatar.to_i == 6 || self.orientation_avatar.to_i == 3
          rotate = self.orientation_avatar.to_i == 6 ? "90" : "180"
          image.combine_options do |b|
            b.rotate rotate
          end
          self.orientation_avatar = ''
          self.avatar = File.open(image.path)
        end
      end
    end
  end

  def active_subscription?
    self.role != DEFAULT_ROLE || (self.active_until.present? && self.active_until.to_date >= Date.today)
  end

  def days_left_subscription
    if self.active_until.present?
      (self.active_until.to_date - Date.today.to_date).to_i
    else
      0
    end
  end

  def become_member
    self.role = DEFAULT_ROLE if self.role == SUPPORTER_ROLE
    self.onboarding = false
    self.onboarding_step = "intro"
    self.plan = nil
    self.active_until = Date.today + 30.days
    self.save
  end

  def update_subscription(days, plan)
    int_days = days.to_i rescue 0
    res = self.active_until.present? ? (self.active_until + int_days.days) : (Date.today + int_days.days)

    # ugrade role from supporter to user
    self.role = DEFAULT_ROLE if self.role == SUPPORTER_ROLE
    self.plan = plan

    self.active_until = res
    self.save
  end

  def current_suggestion
    query = self.suggestion_users.where(:state => 1)
    query.exists? ? query.first : nil
  end

  def current_suggestions
    self.suggestion_users.where("state = 1 or state = 8").order(:created_at)
  end

  def completed_suggestions_days_ago(days_ago)
    self.suggestion_users.where(end_date: days_ago.days.ago..DateTime.now)
  end

  def retry_suggestion
    query = self.suggestion_users.where(:state => 8)
    query.exists? ? query.first : nil
  end

  def suggestions_to_rate
    self.suggestion_users.where(:state => 2, :like => nil)
  end

  def last_suggestion
    #needs to be reviewed because of suggestion user already created and doint again (or retry)
    #self.suggestion_users.order("updated_at").last
    self.suggestion_users.where("state = 1 or state = 8").order("updated_at").last
  end

  def created_suggestions
    Suggestion.where(:user_id => self.id)
  end

  def shared_suggestions
    SuggestionUser.where(:supporter_id => self.id)
  end

  def friend_shared_suggestions(supporter_id)
    self.suggestion_users.where(:supporter_id => supporter_id)
  end

  def comments
    SuggestionUserComment.where(:user_id => self.id)
  end

  def comments_made
    self.comments.where.not("suggestion_user_id in (?)", self.suggestion_users.select {|sug_user| sug_user.id if sug_user.user_id == self.id})
  end

  def number_comments_in_active_goal
    if self.active_goal.present? and self.active_goal.goal.present?
      suggestion_user = self.current_suggestions.map {|sug| sug if sug.suggestion.matches_goal?(self.active_goal.goal)}.compact
      SuggestionUserComment.where("suggestion_user_id in (?)", suggestion_user.map{|sug| sug.id})
    end
  end

  def suggestions_finished
    self.suggestion_users.where.not(start_date:nil, end_date: nil)
  end

  def average_time_complete_suggestion
    total_diff = 0
    sugg_finished = self.suggestions_finished
    sugg_finished.each do |suggestion|
      total_diff += (suggestion.end_date - suggestion.start_date)
    end
    sugg_finished.size >= 1 ? (total_diff/sugg_finished.size/3600).round(1) : 0
  end

  def average_number_complete_suggestion(date_type)
    sugg_finished = self.suggestions_finished
    if sugg_finished.empty?
      return 0
    end
    first_date = sugg_finished.order(:start_date).first.start_date
    last_date = sugg_finished.order(end_date: :desc).first.end_date
    num = sugg_finished.size
    WellclubPersistence::Helper.average_calculation_date(date_type, first_date, last_date, num)
  end

  def suggestions_time_frame(start_date, end_date)
    if start_date.nil?
      return []
    end
    if end_date.nil?
      end_date = DateTime.now
    end
    if end_date < start_date
      end_date, start_date = start_date, end_date
    end
    self.suggestion_users.where(:start_date => start_date..end_date, :end_date => start_date..end_date)
  end

  def average_mood_entries(date_type)
    moods = self.mood_histories.order(:created_at)
    if moods.empty?
      return 0
    end
    first_date = moods.first.created_at
    last_date = moods.last.created_at
    num = moods.size
    WellclubPersistence::Helper.average_calculation_date(date_type, first_date, last_date, num)
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

  def check_invites
    sups = Supporter.where("LOWER(friend_email) = LOWER('#{self.email}')")

    if sups.exists?
      sups.each do |sup|
        if sup.user.present?
          sup.update_attribute(:friend_id, self.id)
        else
          sup.update_attribute(:user_id, self.id)
        end
      end
    end
  end

  def expire_token
    token = Token.where("email = '#{self.email}' and user_id = #{self.id} and role_admin is not null")
    token = Token.where(:email => self.email, :user_id => nil) if token.blank?

    if token.present?
      token = token.last

      if token.present? and (token.role.present? or token.role_admin.present?)
        self.role = token.role if token.role.present?
        if token.role_admin.present?
          self.role_admin = token.role_admin
          #self.role = token.role_admin
        end
        self.save
      end

      token.user = self
      token.save
    end
  end

  def role_down?(base_role)
    return false if role.blank?
    ROLES.index(base_role.to_s) <= ROLES.index(role)
  end

  def role?(base_role)
    return false if role.blank?
    ROLES.index(base_role.to_s) >= ROLES.index(role)
  end

  def role_admin_down?(base_role)
    return false if role_admin.blank?
    ROLES_ADMIN.index(base_role.to_s) <= ROLES_ADMIN.index(role_admin)
  end

  def role_admin?(base_role)
    return false if role_admin.blank?
    ROLES_ADMIN.index(base_role.to_s) >= ROLES_ADMIN.index(role_admin)
  end

  def self.roles_admin(role = nil)
    return ROLES_ADMIN if role.blank?

    result_array = []
    index = ROLES_ADMIN.index(role)
    result_array = ROLES_ADMIN[ROLES_ADMIN.index(role)+1..ROLES_ADMIN.length-1] if(index >= 0)
    result_array

  end

  def ensure_authentication_token
    self.authentication_token = generate_authentication_token if authentication_token.blank?
  end

  def update_token
    generate_authentication_token_api
  end

  def set_default_url
    'default-user.png'
  end

  def invite_supporters
    invited_users = []
    supporter_user_last = {}
    self.message ||= ''

    self.invited_supporters.each do |sup|
      #sup = self.invited_supporter
      # check if person exists in our database
      next if self.email == sup

      sup = sup.downcase

      u = User.find_by(email: sup)

      if u
        # user already exists on our database and check invited status
        user_check = self.supporters.where(:friend => u).order(:created_at)
        if (!user_check.exists? or (user_check.exists? and !user_check.last.state.eql? 'accepted'))
          # not a supporter
          if(user_check.blank?)
            supporter_user_last = Supporter.create(:user => self, :friend => u, :friend_email => sup)
            self.supporters << supporter_user_last
          else
            supporter_user_last = user_check.last
          end
          NotificationDispatcher.send_notification_supporters(supporter_user_last, "invite_supporter_existing", self.message)

          invited_users << supporter_user_last
          #UserMailer.invite_supporter_existing(self, u).deliver
        end
      else
          elem = Supporter.where(:user => self, :friend_email => sup)
          if (!elem.exists?)

            supporter_user_last = Supporter.create(:user => self, :friend_email => sup, :token => generate_token_supporter(sup))
            self.supporters << supporter_user_last

            SendEmailWorker.perform_async(supporter_user_last.id, "invite_supporter", self.message)

            invited_users << supporter_user_last
          else
            supporter_user_last = self.supporters.where(:friend => u).last
          end
      end
    end

    invited_users
  end


  def invite_members
    invited_users = []
    supporter_user_last = {}#
    self.message ||= ''
    self.invited_supportees.each do |sup|
      #sup = self.invited_supporter
      # check if person exists in our database
      next if self.email == sup

      sup = sup.downcase

      u = User.find_by(email: sup)

      if u
        # user already exists on our database and check invited status
       # user_check = Supporter.where(:user => u, :friend => self)
          # not a supporter
        supporter_user_last = Supporter.create(:friend => self, :user => u, :friend_email => sup, :member_invite => true)
        NotificationDispatcher.send_notification_members(supporter_user_last, "invite_member_existing", self.message)#, self.message)
        invited_users << supporter_user_last
          #UserMailer.invite_supporter_existing(self, u).deliver
       else
          elem = Supporter.where(:friend => self, :friend_email => sup)
          if (elem.exists.blank?)
            supporter_user_last = Supporter.create(:friend => self, :friend_email => sup, :token => generate_token_supporter(sup, false), :member_invite => true)
            SendEmailWorker.perform_async(supporter_user_last.id, "invite_member", self.message)
            invited_users << supporter_user_last
          else
            supporter_user_last = self.supporters.where(:friend => u).last
          end
      end
    end

    invited_users
  end



  def show_mood_tile?
    return true  if self.last_mood_date.nil?
    self.last_mood_date < DateTime.now.beginning_of_day
  end

  def show_weight_tile?

    if self.has_active_goal? && self.active_goal.goal.tracking_type == "weight"
      return true  if self.last_weight_date.nil?
      self.last_weight_date < DateTime.now.beginning_of_day
    end

    false
  end

  def last_weight
    if self.weight_histories.present?
      self.weight_histories.last.weight
    else
      nil
    end
  end

  def set_start_weight
    if self.weight_changed? and self.start_weight.blank?
      self.start_weight = self.weight
    end
  end

  def populate_fields
    self.email = self.email.downcase
  end

  def save_mood_history
    if mood_changed?
      mood_history = MoodHistory.new
      mood_history.mood = self.mood
      mood_history.user = self
      mood_history.save
    end
  end

  def save_weight_history
    if weight_changed?
      if self.has_active_goal? && self.active_goal.goal.tracking_type == "weight"
        if last_weight.present?
          delta = last_weight - self.weight
        else
          if self.start_weight.present?
            delta = self.start_weight - self.weight
          else
            delta = 0
          end
        end
        self.active_goal.update_progress(delta)
      end

      weight_history = WeightHistory.new
      weight_history.weight = self.weight
      weight_history.user = self
      weight_history.save
    end

    if start_weight_changed?
      weight_history = self.weight_histories.order('created_at')
      weight_history = (weight_history.present? ? weight_history.first : WeightHistory.new)
      weight_history.weight = self.start_weight
      weight_history.user = self
      weight_history.save
    end
  end

  def accepted_supporters
    self.supporters.where(:state => 'accepted')
  end

  def accepted_members
    Supporter.where(:friend_id => self.id, :state => 'accepted')
  end

  def get_supporters
    Supporter.where(:user_id => self.id).where("state <> 'denied' or state is null")
  end

  #legacy problems, now "team" notion exists instead of supporters and supportees
  def get_supportees
    if (get_supporters.present?)
      Supporter.where(:friend_id => self.id).where("(state <> 'denied' or state is null) and user_id not in (?)", self.get_supporters.where("friend_id is not null").map{|elem| elem.friend_id})
    else
      Supporter.where(:friend_id => self.id).where("(state <> 'denied' or state is null)")
    end
  end

  def get_team
    (self.get_supporters.order("created_at desc") + self.get_supportees.order("created_at desc")).uniq
  end

#ONBOARDING
  def complete_onboarding(step)
    self.onboarding = true
    self.onboarding_step = step
    self.save
  end

  def current_step
    @current_step || steps.first
  end

  def steps
    %w[goal preferences suggestions invite_supporter]
  end

  def next_step
    self.onboarding_step = steps[steps.index(onboarding_step)+1]
  end

  def previous_step
    self.onboarding_step = steps[steps.index(onboarding_step)-1]
  end

  def first_step?
    onboarding_step == steps.first
  end

  def last_step?
    onboarding_step == steps.last
  end

  def all_valid?
    steps.all? do |step|
      self.onboarding_step = step
      valid?
    end
  end

  def first_name
    self.name.split(' ').first rescue self.name
  end

  def has_active_goal?
    self.goal_users.where(:status => "started").exists?
  end

  def has_completed_goal?
    return false if !self.goal_users.present? || self.has_active_goal?
    self.goal_users.last.status == "completed"
    #self.goal_users.where(:status => "completed").exists?
  end

  def active_goal
    self.goal_users.where(:status => "started").first
  end

  def initialized_goal
    self.goal_users.where(:status => "initialized").first
  end

  def completed_goal
    self.goal_users.where(:status => "completed").last
  end

  def safe_name
    ((self.role_admin?(:professional) and self.role?(:professional)) ? "#{self.name} (Wellclub Support)" : self.name)
  end

  def self.is_a_valid_emails?(emails)
      emails.each do |email|
        return false if !User.is_a_valid_email?(email)
      end
      return true
  end

  def self.is_a_valid_email?(email)
    (Devise::email_regexp.match email).present?
  end

  def invite_admin_user(email, role = nil)
      SendEmailWorker.perform_async(generate_token_admin(email,role).id, "invite_admin")
  end

  # Callback to overwrite if confirmation is required or not.
  def confirmation_required?
     false #((self.role.eql? DEFAULT_ROLE) && (self.role_admin.blank?))
  end

  def send_feedback(message)
    SendEmailWorker.perform_async(self.id, "feedback", message)
  end

  # Suggestion Engine
  def suggestion_feed(number=5, session= "123", ids_to_exclude = nil)

    session = "123" if session.nil? #static value?
    doing_suggestions = self.suggestion_users.where("state = 1").map{|s| s.suggestion_id }
    sent_suggestions = self.suggestion_users.where.not(suggestion_id: doing_suggestions).where("state = 4").map{|s| s.suggestion_id }.uniq

    likes = self.user_tags.where(:like => true).map{|elem| elem.tag.name}
    dislikes = self.user_tags.where(:like => false).map{|elem| elem.tag.name}

    problems = self.has_active_goal? ? self.active_goal.goal.subproblems.map(&:identifier) : []

    completed_suggestions = self.suggestion_users.where("(state = 2 or state = 3) AND end_date > ?", Time.now-21.days).map{|s| s.suggestion_id }
    rejected_suggestions = self.suggestion_users.where("state = 6").map{|s| s.suggestion_id }

    rejected_suggestions = rejected_suggestions + ids_to_exclude if ids_to_exclude.present?
    rejected_suggestions = rejected_suggestions + doing_suggestions

    skipped_suggestions = self.suggestion_users.where("state = 7 AND end_date > ?", Time.now-7.days).map{|s| s.suggestion_id }

    results = {}
    if active_goal.nil?  or sent_suggestions.count > number-1
      Suggestion.search( RecAlgorithm.basic_search(number, sent_suggestions, likes, dislikes, problems, completed_suggestions, rejected_suggestions, skipped_suggestions, session)).records.each_with_hit{|s, h| results[s] = h }
    else
      number_suggestions = (sent_suggestions - rejected_suggestions).count

      if number_suggestions > 0
          Suggestion.search( RecAlgorithm.basic_search(number_suggestions, sent_suggestions, [], [], [], completed_suggestions, rejected_suggestions, skipped_suggestions, session)).records.each_with_hit{|s, h| results[s] = h }
      end

      #when it gets the some suggestion
      rejected_suggestions = (rejected_suggestions + results.map{|elem| elem.first.id}) if results.present?

      if  active_goal.goal.tracking_type.eql? "suggestion"
          main_num = number * 0.6
          other_num = number * 0.2

          Suggestion.search( RecAlgorithm.basic_search_weight(main_num, sent_suggestions, likes, dislikes, problems, completed_suggestions, rejected_suggestions, skipped_suggestions, session, active_goal.goal.lifestyle_category)).records.each_with_hit{|s, h| results[s] = h }
          categories = LifestyleCategory::CATEGORIES.keys.reject {|n| n == active_goal.goal.lifestyle_category.to_sym || n == :miscellaneous}
          categories.each do |category|
            Suggestion.search( RecAlgorithm.basic_search_weight(other_num, sent_suggestions, likes, dislikes, problems, completed_suggestions, rejected_suggestions, skipped_suggestions, session, category.to_s)).records.each_with_hit{|s, h| results[s] = h }
          end
      else
          main_num = number * 0.4
          other_num = number * 0.2
          Suggestion.search( RecAlgorithm.basic_search_weight(main_num, sent_suggestions, likes, dislikes, problems, completed_suggestions, rejected_suggestions, skipped_suggestions, session, "eating")).records.each_with_hit{|s, h| results[s] = h }
          Suggestion.search( RecAlgorithm.basic_search_weight(main_num, sent_suggestions, likes, dislikes, problems, completed_suggestions, rejected_suggestions, skipped_suggestions, session, "movement")).records.each_with_hit{|s, h| results[s] = h }
          Suggestion.search( RecAlgorithm.basic_search_weight(other_num, sent_suggestions, likes, dislikes, problems, completed_suggestions, rejected_suggestions, skipped_suggestions, session, "self_view")).records.each_with_hit{|s, h| results[s] = h }
      end

      results = results.sort_by { |k, v| v._score }.reverse.first(number)
    end
    results
  end

  def suggestion_elastic_search(number=5, session= "123", lifestyleCategory)
    doing_suggestions = self.suggestion_users.where("state = 1").map{|s| s.suggestion_id }
    sent_suggestions = self.suggestion_users.where.not(suggestion_id: doing_suggestions).where("state = 4").map{|s| s.suggestion_id }.uniq
    likes = self.user_tags.where(:like => true).map{|elem| elem.tag.name}
    dislikes = self.user_tags.where(:like => false).map{|elem| elem.tag.name}
    problems = self.has_active_goal? ? self.active_goal.goal.subproblems.map(&:identifier) : []
    completed_suggestions = self.suggestion_users.where("(state = 2 or state = 3) AND end_date > ?", Time.now-21.days).map{|s| s.suggestion_id }
    rejected_suggestions = self.suggestion_users.where("state = 6").map{|s| s.suggestion_id }
    rejected_suggestions = rejected_suggestions + doing_suggestions
    skipped_suggestions = self.suggestion_users.where("state = 7 AND end_date > ?", Time.now-7.days).map{|s| s.suggestion_id }
    results = {}

    Suggestion.search( RecAlgorithm.basic_search_weight(number, sent_suggestions, likes, dislikes, problems, completed_suggestions, rejected_suggestions, skipped_suggestions, session, lifestyleCategory)).records.each_with_hit{|s, h| results[s] = h }
    results = results.sort_by { |k, v| v._score }.reverse.first(number)
    results.map{|elem| elem.first}
  end

  def suggestion_search(number = 5, order_by)
    Suggestion.where("id not in (?)",  get_not_include).order(order_by).limit(number)
  end

  def suggestion_user_search(number = 5, order_by)
    results = SuggestionUser.where("suggestion_id not in (?)", get_not_include).order(order_by).map{|elem| elem.suggestion}.uniq
    results[0, number]
  end

  def get_not_include
    doing_suggestions = self.suggestion_users.where("state = 1").map{|s| s.suggestion_id }
    completed_suggestions = self.suggestion_users.where("(state = 2 or state = 3) AND end_date > ?", Time.now-21.days).map{|s| s.suggestion_id }
    rejected_suggestions = self.suggestion_users.where("state = 6").map{|s| s.suggestion_id }
    rejected_suggestions = rejected_suggestions + doing_suggestions

    skipped_suggestions = self.suggestion_users.where("state = 7 AND end_date > ?", Time.now-7.days).map{|s| s.suggestion_id }
    not_include = doing_suggestions + completed_suggestions + rejected_suggestions + skipped_suggestions
    not_include
  end

  def can_generate_quote?
    return true  if self.last_quote_date.nil?
    self.last_quote_date  < DateTime.now.beginning_of_day
  end

  def can_generate_daily_nugget?
    return true  if self.last_daily_nugget_date.nil?
    self.last_daily_nugget_date  < DateTime.now.beginning_of_day
  end

  def get_quote
    quote = self.quote

    if self.can_generate_quote?
      quote = TileMessage.where.not(id: quote.id).where(:message_type => 'quote').order("RANDOM()").limit(1).first if quote.present?
      quote = TileMessage.where(:message_type => 'quote').order("RANDOM()").limit(1).first unless quote.present?
      self.quote = quote
      self.last_quote_date = DateTime.now
      self.save
    end

    quote
  end

  def get_daily_nugget
    daily_nugget = self.daily_nugget

    if self.can_generate_daily_nugget?
      daily_nugget = TileMessage.where.not(id: daily_nugget.id).where(:message_type => 'daily_nugget').order("RANDOM()").limit(1).first if daily_nugget.present?
      daily_nugget = TileMessage.where(:message_type => 'daily_nugget').order("RANDOM()").limit(1).first unless daily_nugget.present?
      self.daily_nugget = daily_nugget
      self.last_daily_nugget_date = DateTime.now
      self.save
    end

    daily_nugget
  end

  # TO BE IMPROVED
  def show_motivation_tile?
    self.last_motivation_tile_date <= Date.today - 6.days or self.last_motivation_tile_date == Date.today
  end

  def show_reward_tile?
    self.last_reward_tile_date <= Date.today - 6.days or self.last_reward_tile_date == Date.today
  end

  def show_goal_progress_tile?
    self.last_progress_goal_tile_date <= Date.today - 3.days or self.last_progress_goal_tile_date == Date.today
  end

  def show_recent_complete_tile?
    self.last_recent_completed_tile_date <=  Date.today - 7.days or self.last_recent_completed_tile_date == Date.today
  end

  def show_mood_graph_tile?
    self.last_mood_graph_tile_date <=  Date.today - 7.days or self.last_mood_graph_tile_date == Date.today
  end

  def show_suggestion_credit?
    date = Date.today
    self.next_suggestion_credit_date >=  date.beginning_of_week and self.next_suggestion_credit_date <= date.end_of_week
  end

  # TO BE IMPROVED
  def can_get_motivation_tile?
    if (self.last_motivation_tile_date.nil?)
       self.last_motivation_tile_date = Date.today - 5.days#default value
       self.save
       return false
    else
      if show_motivation_tile?
       self.last_motivation_tile_date = Date.today
       self.save
       return true
      end
    end
    return false
  end

  def can_get_reward_tile?
    if (self.last_reward_tile_date.nil?)
       self.last_reward_tile_date = Date.today - 2.day #default value
       self.save
       return false
    else
      if show_reward_tile?
       self.last_reward_tile_date = Date.today
       self.save
       return true
      end
    end
    return false
  end

  def can_get_goal_progress_tile?
    if (self.last_progress_goal_tile_date.nil?)
       self.last_progress_goal_tile_date = Date.today - 2.days #default value
       self.save
       return false
    else
      if show_goal_progress_tile?
       self.last_progress_goal_tile_date = Date.today
       self.save
       return true
      end
    end
    return false
  end

  def can_get_recent_complete_tile?
    if (self.last_recent_completed_tile_date.nil?)
       self.last_recent_completed_tile_date = Date.today - 1.day #default value
       self.save
       return false
    else
      if show_recent_complete_tile?
       self.last_recent_completed_tile_date = Date.today
       self.save
       return true
      end
    end
    return false
  end

  def can_get_mood_graph_tile?
    if (self.last_mood_graph_tile_date.nil?)
       self.last_mood_graph_tile_date = Date.today - 1.day #default value
       self.save
       return false
    else
      if show_mood_graph_tile?
        self.last_mood_graph_tile_date = Date.today
        self.save
        return true
      end
    end
    return false
  end

  def can_get_suggestion_credit?
    if (self.next_suggestion_credit_date.nil?)
       self.next_suggestion_credit_date = Date.today+7.day #default value
       self.save
       return false
    else
       return show_suggestion_credit?
    end
    return false
  end

  def can_show_suggestion_help
    return self.dismissed_suggestion_how_it_works_date.blank?
  end

  def dismiss_suggestion_credit
    date = (DateTime.now + 7.days)
    self.next_suggestion_credit_date = User.rand_time(date.beginning_of_week, date.end_of_week)
    self.save
  end

  def dismiss_mood_overtime
    self.last_mood_graph_tile_date = DateTime.now - 1.day
    self.save
  end

  def dismiss_nugget
    self.daily_nugget = nil
    self.save
  end

  def dismiss_suggestion_help
    self.dismissed_suggestion_how_it_works_date = Date.today
    self.save
  end

  def get_photos_team(from)
    hash = {}
    team_ids = self.get_supporters.select{|elem| elem != nil and elem.friend != nil}.map{|elem| elem.friend.id}
    team_ids = team_ids + self.get_supportees.select{|elem| elem != nil and elem.user != nil}.map{|elem| elem.user.id}
    from_where = ''
    if from.present?
      from_where = "and end_date > '#{from}'"
    end

    SuggestionUser.where("user_id in (?) and photo_file_name is not null #{from_where}", team_ids).group_by{ |u| u.end_date.strftime("%V") }.map{|k, v| hash[k] = v.map{|elem| elem.photo(:thumb)}}
    hash

  end

  def self.rand_time(from, to=Time.now)
    Time.at(rand_in_range(from.to_f, to.to_f))
  end

  def self.rand_in_range(from, to)
    rand * (to - from) + from
  end

  # user wizard
  def profile_complete?
    self.user_tags.present? && self.has_active_goal? && self.friends.present?
  end

  private

  def destroy_supporters
    Supporter.where("user_id = #{self.id} or friend_id = #{self.id}").destroy_all
    SuggestionUser.where("supporter_id = #{self.id}").destroy_all
  end

  def set_default_role
    self.role ||= DEFAULT_ROLE
  end

  def set_trial
    if self.role == DEFAULT_ROLE
      self.active_until = (Time.now + 3.months)
      self.plan = nil
    end
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

  def generate_token_supporter(email, supporter = true)
    token = Token.new
    token.supporter = supporter
    token.email = email
    token.save
    token
  end

  def generate_token_admin(email, role = nil)
    token = Token.new
    token.admin = true
    token.email = email
    token.user = User.where(:email => email).first if User.where(:email => email).present?
    token.role_admin = role if role.present?
    token.save
    token
  end

end
