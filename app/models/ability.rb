class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update ankd :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
    @user = user
    @user ||= User.new
    alias_action :create, :read, :update, :destroy, :to => :crud
    #one rule
    send(@user.role_admin)
    #multiple roles
    #    user.roles.each { |role| send(role) }
    #if @user.roles.size == 0
    #  can :read, :all #for guest without roles
    #end

  end


# ADMIN
  def super_admin
   client_admin
   can :manage, User
   can :manage, GoalSuggestionType
   can :manage, Notification
   can :manage, Supporter
   can :manage, Tag
   can :manage, TileMessage
   can :manage, UserTag
   can :manage, UserProperty
   can :manage, Property
  end

  def client_admin
    editor
    can :manage, Goal
    can :manage, GoalTask
    can :manage, GoalUser
    can :manage, Problem
    can :manage, Subproblem
    can :manage, Plan
    can :manage, Coupon
    can :manage, Payment
    can :manage, User, :role => "editor"
    can :manage, User, :role => "member"
    can :manage, User, :role => "supporter"
  end

  def editor
    writer
    can :manage, Tag
    can :manage, User, :role => "writer"
    can :manage, User, :role => "professional"
    can :share, SuggestionUser
  end

  def writer
    professional
    can :manage, SuggestionUser
    cannot :share, SuggestionUser
    can :manage, SuggestionUserComment
    cannot :manage, User
    can :edit, User, :id => @user.id
    can :manage, TileMessage
  end

  def professional
   can :manage, Suggestion
   can :manage, SuggestionType
   can :manage, SuggestionsTag
   can :manage, Playlist
   can :manage, PlaylistSuggestion
  end

# APP
  def member
  end

  def supporter
  end

end
