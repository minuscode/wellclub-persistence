class UserProperty < ActiveRecord::Base
  # Relationships
  belongs_to :user
  belongs_to :property
end
