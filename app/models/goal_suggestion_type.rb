class GoalSuggestionType < ActiveRecord::Base
  belongs_to :goal
  belongs_to :suggestion_type
end
