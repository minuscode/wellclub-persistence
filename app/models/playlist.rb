class Playlist < ActiveRecord::Base
	# Relationships
	has_many :playlist_suggestions
	has_many :suggestions, :through => :playlist_suggestion	

	validates_presence_of :name, :description
end
