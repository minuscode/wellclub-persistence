class Problem < ActiveRecord::Base
  # Relationships
  has_many :goals, :through => :goal_problems
  has_many :goal_problems

  has_many :suggestions, :through => :suggestion_problems
  has_many :suggestion_problems

  has_many :subproblems

  enum lifestyle_category: LifestyleCategory::CATEGORIES.keys

  validates_presence_of :category, :identifier, :lifestyle_category
end
