class Property < ActiveRecord::Base
	# Relationships
	has_many :user_properties
	has_many :users, :through => :user_properties
	has_many :tags, :through => :user_properties_tag
end
