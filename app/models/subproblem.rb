class Subproblem < ActiveRecord::Base
  belongs_to :problem

  has_many :suggestions, :through => :suggestion_subproblems
  has_many :suggestion_subproblems

  after_update { self.suggestions.each(&:touch) }

  def full_name
    "#{identifier} - #{category}"
  end

  def self.import_csv(file)
    CSV.foreach(file, {:col_sep => ";"}) do |row|
      if $. == 1
        # first column validation
        return "Invalid CSV File format" unless (row[0] == "Lifestyle Category" && row[1] == "LC Identifier")

      else

        next if Subproblem.where(:identifier => row[5]).exists?

        problem = Problem.find_or_create_by(identifier: row[3], category: row[2], lifestyle_category: LifestyleCategory::CATEGORIES.values.index(row[1]))

        #Lifestyle Category, LC Identifier, Problem Category, PC Identifier, Problem, Identifier
        Subproblem.create(
          :identifier => row[5],
          :category => row[4],
          :problem => problem
        )
      end
    end
  end

end
