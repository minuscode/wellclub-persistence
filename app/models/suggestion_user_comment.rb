class SuggestionUserComment < ActiveRecord::Base
  belongs_to :user
  belongs_to :suggestion_user
  after_commit :send_notifications, :on => :create

  attr_accessor :dont_send_comment

  def send_notifications
    #suggestion started and share state
    if self.suggestion_user.state == 1 or (self.suggestion_user.state == 4 and @dont_send_comment.blank?)
      NotificationDispatcher.send_notification_comment(self)
    end

    NotificationDispatcher.send_notification_comment_to_supporters(self)
  	#UserMailer.suggestion_comment(self).deliver
  end

  def self.create_comment(comment_text, user, suggestion_user, dont_send_comment=false)
    comment = SuggestionUserComment.new
    comment.suggestion_user = suggestion_user
    comment.comment = comment_text
    comment.user = user
    comment.dont_send_comment= dont_send_comment
    comment.save
  end

  def self.receive_email_comment(user_email, sugg_user, message)
    user = User.where(:email => user_email)
    sugg_user = SuggestionUser.where(:id => sugg_user)

    parsed_body = WellclubPersistence::Helper.get_string_between(message, "<div", "</div>")
    parsed_body = "<div #{parsed_body}</div>"
    parsed_body = ActionView::Base.full_sanitizer.sanitize(parsed_body)

    # gmail test fix
    if !user.exists?
      if user_email.present?
        blocks = user_email.split("@")
        blocks[0] = blocks[0].gsub('.','')
        merged_mail = blocks.join('@')
        user = User.where(:email => merged_mail)
      end
    end

    if user.exists? && sugg_user.exists?
      user = user.first
      sugg_user = sugg_user.first
      SuggestionUserComment.create_comment(parsed_body, user, sugg_user) if parsed_body.present?
      puts "TO:#{sugg_user.user.email}, FROM:#{user_email}, MESSAGE: #{parsed_body}"
    else
      puts "COULDNT SEND! FROM:#{user_email}, SUGUSER #{sugg_user.id}, MESSAGE: #{parsed_body}"
    end
  end
end
