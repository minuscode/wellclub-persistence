class PlaylistSuggestion < ActiveRecord::Base
  # Relationships
  belongs_to :suggestion
  belongs_to :playlist
end
