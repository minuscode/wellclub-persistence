class TileMessage < ActiveRecord::Base
  belongs_to :user

  before_save :check_processor, :check_processor_new
  before_create :check_processor_new

  enum processors: [ :youtube, :image, :link]

  attr_reader :cover_from_url
  has_attached_file :cover, Application::SUGGESTION_PAPERCLIP_OPTS.merge({
    :styles => {
      :thumb   => 'x100',
      :mobile   => 'x400',
      :desktop => 'x600'
    },
    #:default_url => '/assets/defaults/:class/:style/missing.jpg'
    :default_url => lambda { |cover| cover.instance.set_default_url}
    #:convert_options => {
    #  :all => '-blur 0x2'
   #}
  })
  validates_attachment :cover,
    :content_type => { :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  #  :size => { :in => 0..1700.kilobytes }

  #message type
  TYPES = %w[quote daily_nugget]

  def set_default_url
    ActionController::Base.helpers.asset_path('misc/broken-link.png')
  end

  def self.import_csv(file)
    CSV.foreach(file, {:col_sep => ";"}) do |row|
      if $. == 1
        # first column validation
        return "Invalid CSV File format" unless (row[0] == "quote_text" && row[1] == "quote_author")

      else

        next if TileMessage.where(:message => row[0]).exists?

        TileMessage.create(
          :message => row[0],
          :author => row[1],
          :message_type => 'quote'
        )
      end
    end
  end


  def cover_from_url=(url_value)
    self.cover = URI.parse(url_value)
    # Assuming url_value is http://example.com/photos/face.png
    # avatar_file_name == "face.png"
    # avatar_content_type == "image/png"
    @cover_from_url = url_value
  end

  def check_processor
    if self.url_changed?
      self.processor = self.detector
    end
  end

  def check_processor_new
    begin
    if self.url.present?
      self.processor = self.detector
      if self.processor == :image
        self.cover = URI.parse(self.url)
      end
    end
    rescue
    end
  end

  def detector

    if (self.processor != "link" and self.processor != :link)
      return :image if (self.processor == "image" or  self.processor == :image) and self.cover.file?
      return Suggestion.detector(self.url)
    else
      self.processor
    end
  end

  def outputter
    # first detects
    case self.processor
    when "image"
      # prints image link
      res = "<img src='#{self.cover(:mobile)}'>"
    when "youtube"
      # grab youtube id
      regex = /youtube.com.*(?:\/|v=)([^&$]+)/
      yt_id = self.url.match(regex)[1]
      res = "<iframe width='420' height='315' src='//www.youtube.com/embed/#{yt_id}' frameborder='0' allowfullscreen></iframe>"
    when "link"
      res = ActionController::Base.helpers.link_to(url,url, :target=>"_blank")
    else
      # no processor available for this
      res = ""
      res = ActionController::Base.helpers.image_tag(self.cover) if self.cover.present?
    end

    res
  end

end
