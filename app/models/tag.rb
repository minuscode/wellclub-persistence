class Tag < ActiveRecord::Base
  # Relationships
  has_many :user_tags, :dependent => :destroy
  has_many :users, :through => :user_tags

  has_many :suggestions, :through => :suggestions_tags
  has_many :suggestions_tags

  enum lifestyle_category: LifestyleCategory::CATEGORIES.keys

  after_update { self.suggestions.each(&:touch) }

  def self.import_csv(file)
    CSV.foreach(file, {:col_sep => ";"}) do |row|
      if $. == 1
        # first column validation
        return "Invalid CSV File format" unless (row[0] == "Likes/Dislikes Tag" && row[1] == "Identifier")

      else

        next if Tag.where(:identifier => row[0]).exists?

        #Likes/Dislikes Tag, Identifier
        Tag.create(
          :name => row[0],
          :identifier => row[1],
          :lifestyle_category => LifestyleCategory::CATEGORIES.values.index(row[2]),
          :approved => true
        )
      end
    end
  end

end
