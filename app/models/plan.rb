class Plan < ActiveRecord::Base
  has_many :payments
  has_many :users
end
