class Goal < ActiveRecord::Base
  has_attached_file :logo, {:styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => lambda { |logo| logo.instance.set_default_url} }.merge(Application::DEFAULTS)
  validates_attachment_content_type :logo, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  # Relationships
  has_many :tags, :through => :goal_tags
  has_many :goal_tags

  has_many :users, :through => :goal_users
  has_many :goal_users, :dependent => :nullify

  has_many :subproblems, :through => :goal_subproblems
  has_many :goal_subproblems

  after_destroy :cancel_goals
  enum lifestyle_category: LifestyleCategory::CATEGORIES.keys


  def cancel_goals
    GoalUser.where(:goal_id => nil).each do |goal_user|
      goal_user.canceled_goal
      goal_user.save
    end
  end

  def set_default_url
    ActionController::Base.helpers.asset_path('misc/broken-link.png')
  end

  def self.import_csv(file)
    CSV.foreach(file, {:col_sep => ";"}) do |row|
      if $. == 1
        # first column validation
        return "Invalid CSV File format" unless (row[0] == "Healthy Habits" && row[1] == "Problem Categories")
      else
        #next if Goal.where(:name => row[0]).exists?

        #Healthy Habits, Problem Categories, Matching Sub-Sub-Problem Categories
        goal = Goal.find_or_create_by(:name => row[0])

        goal.tracking_type = "suggestion"
        goal.default_amount = "10"
        goal.fixed_amount = true
        goal.unit_amount = "time"
        goal.options_amount = ""
        goal.default_period = "3"
        goal.fixed_period = false
        goal.unit_period = "week"
        goal.options_period = "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24"

        problems = row[2].split(",")

        problems.each do |p_id|
          puts p_id
          problem = Subproblem.find_by(identifier: p_id.strip)

          goal.lifestyle_category = problem.problem.lifestyle_category

          next if !problem.present?

          goal.subproblems << problem if (!goal.subproblems.include?(problem))
        end


        goal.save
      end
    end
  end

end
