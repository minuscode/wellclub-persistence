class Notification < ActiveRecord::Base
  # Relationships
  belongs_to :user
  enum class_type: [
   :suggestion_user_comment,
   :supporter,
   :suggestion_user,
   :goal_user]

  enum message_type:
   [:started_suggestion_supporter,
    :retried_suggestion_supporter,
    :rejected_suggestion_supporter,
    :shared_suggestion,
    :prompt_0_supporter,
    :prompt_1_supporter,
    :prompt_2_supporter,
    :prompt_3_supporter,
    :prompt_f_supporter,
    :prompt_0,
    :prompt_1,
    :prompt_2,
    :prompt_3,
    :prompt_f,
    :suggestion_comment,
    :suggestion_comment_supporter,
    :suggestion_comment_supporter_3rd,
    :prompt_goal_expired,
    :prompt_goal_achieved_supporter,
    :prompt_goal_expired_supporter,
    :supporter_accepted,
    :invite_supporter_existing,
    :custom,
    :invite_member_existing,
    :member_accepted ]

  def self.get_notification(prompt,class_type)
    class_name =  Notification.class_types[class_type.to_s.underscore.to_sym]
    message_type = Notification.message_types[prompt.to_sym]

    notification = Notification.where(:message_type => message_type, :class_type => class_name)
    if notification.blank?
      notification = Notification.create(:message_type => message_type, :class_type => class_name)
    else
      notification = notification.first
    end

    notification
  end

  def create_user_notification(user_id, table_id, notification_type)
    user_notification = UserNotification.new
    user_notification.user_id = user_id
    user_notification.notification = self
    user_notification.notification_type = notification_type
    user_notification.table_id = table_id
    user_notification.save
    self.create_message(user_notification) unless self.message.present?
  end

  def create_message(user_notification)
    message = ''

    case self.class_type.to_sym
    when :suggestion_user
      suggestion_user = user_notification.object
      if suggestion_user.user == user_notification.user
        message = "suggests"
      else
        #special case with 2 status
        if self.message_type == "prompt_f"
          message = "#status your suggestion"
        else
          message = "#{suggestion_user.get_state.downcase} your suggestion"
        end
      end
    when :suggestion_user_comment
      message = ""
    when :supporter
      if self.message_type == "invite_member_existing"
        message = "wanted to be your supporter"
      elsif self.message_type == "invite_supporter_existing"
        message = "wanted your support"
      elsif self.message_type == "member_accepted"
        message = "accepted your request"
      else
        message = "accepted your support request"
      end
    when :goal_user
      if self.message_type == "prompt_goal_expired"
        message = "has expired"
      else
        message = "has been achieved"
      end
    end

    self.message = message
    self.save
  end

end
