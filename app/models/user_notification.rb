class UserNotification < ActiveRecord::Base
  belongs_to :user
  belongs_to :notification
  enum notification_type: [:desktop, :email, :sms]

  def object
    klass = Object.const_get self.notification.class_type.camelize
    klass.find_by_id(self.table_id)
  end

end
