class RecAlgorithm
  def self.basic_search(limit = 10, ids, likes, dislikes, problems, completed, rejected, skipped, seed)
    {
       from: 0,
       size: limit,
       query: {
       function_score: {
            query: { match_all: {} },
            filter: {
              or: [ { term: { approved: "true" } }, { ids: { values: ids } } ]
            },
            boost: 1,
            functions: [
              {
                filter:{
                  ids: { values: ids }
                },
                boost_factor: 4*10
              },
              {
                filter: {
                   terms: {tags: likes}
                },
                boost_factor: 0.3*10
              },
              {
                filter: {
                   terms: {tags: dislikes}
                },
                boost_factor: -100
              },
              {
                filter: {
                   terms: {problems: problems}
                },
                boost_factor: 2*10
              },
              {
                filter: {
                  ids: { values: completed }
                },
                boost_factor: -10
              },
              {
                filter: {
                  ids: { values: rejected }
                },
                boost_factor: -100
              },
              {
                filter: {
                  ids: { values: skipped }
                },
                boost_factor: -10
              },
              {
                random_score: {
                  seed: seed.to_i
                }
              }
            ],
            score_mode: "sum"
          }
        }

    }
  end

 def self.basic_search_weight(limit = 2, ids, likes, dislikes, problems, completed, rejected, skipped, seed, lifestyle_category)
    {
       from: 0,
       size: limit,
       query: {
       function_score: {
            query: { match_all: {} },
            filter: {
                bool: {
                  must: {
                      term: { lc_cat: lifestyle_category }
                  },
                  should: [
                        {
                          term: { approved: "true" }
                        },
                        {
                          ids: { values: ids }
                        }
                  ]
                }
            },
            boost: 1,
            functions: [
              {
                filter:{
                  ids: { values: ids }
                },
                boost_factor: 4*10
              },
              {
                filter: {
                   terms: {tags: likes}
                },
                boost_factor: 0.3*10
              },
              {
                filter: {
                   terms: {tags: dislikes}
                },
                boost_factor: -100
              },
              {
                filter: {
                   terms: {problems: problems}
                },
                boost_factor: 2*10
              },
              {
                filter: {
                  ids: { values: completed }
                },
                boost_factor: -10
              },
              {
                filter: {
                  ids: { values: rejected }
                },
                boost_factor: -100
              },
              {
                filter: {
                  ids: { values: skipped }
                },
                boost_factor: -10
              },
              {
                random_score: {
                  seed: seed.to_i
                }
              }
            ],
            score_mode: "sum"
          }
        }
    }


  end


  def self.test_boost(ids)
    {
      size: 100,
      query: {
        function_score: {
          query: {match_all: {} },
          boost: 1,
          functions: [
            {
              filter:{
                ids: { values: ids }
              },
              boost_factor: 2
            }
          ],
          max_boost: 7,
          score_mode: "sum",
        }
      }
    }
  end

  def self.test_filter(ids)
    {
       size: 100,
       query: {
          filtered: {
            query: { match_all: {} },
              filter: {
                not: {
                  ids: { values: ids }
                }
              }
          }
       }
    }
  end

  def self.test
    {
      size: 100,
      query: {
        function_score: {
          query: {match_all: {} },
          boost: 1,
          functions: [
          ],
          max_boost: 7,
          score_mode: "sum",
        }
      }
    }
  end


  def self.trendy_n_pricey
    {
      query: {
        function_score: {
          query: {match_all: {} },
          boost: 1,
          functions: [
            {
              script_score: {
                script: "_score + 2 * doc['trendiness'].value + 0.5 * doc['price'].value"
              }
            }
          ],
          max_boost: 7,
          score_mode: "sum",
        }
      }
    }
  end

  def self.trendy_n_pricey_n_greenlit
    {
      query: {
        function_score: {
          filter: {ids: {values: [1,3,5,7,9,11,13,18,22,25,43,67,82,83,99 ] } },
          boost: 1,
          functions: [
            {
              script_score: {
                script: "_score + 2 * doc['trendiness'].value + 0.5 * doc['price'].value"
              }
            }
          ],
          max_boost: 7,
          score_mode: "sum",
        }
      }
    }
  end

=begin
  def self.search(query)
    __elasticsearch__.search \
          size: 100,
          query: {
            multi_match: {
              query:    query,
              fields: [ 'lc_cat^100', 'name^1', 'problems^50' ]
            }
          }
  end

  def self.search(query)
    __elasticsearch__.search(
      {
        query: {
          multi_match: {
            query: query,
            fields: ['lc_cat^10']
          }
        }
      }
    )
  end
=end

end
