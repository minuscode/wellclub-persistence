# encoding: UTF-8
require 'elasticsearch/model'
class Suggestion < ActiveRecord::Base
  extend FriendlyId
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  enum processors: [ :image, :youtube, :link]
  enum lifestyle_category: LifestyleCategory::CATEGORIES.keys

  attr_accessor :members, :comment

  attr_reader :cover_from_url
  has_attached_file :cover, Application::SUGGESTION_PAPERCLIP_OPTS.merge({
    :styles => {
      :thumb   => 'x100',
      :mobile   => 'x400',
      :desktop => 'x600'
    },
    #:default_url => '/assets/defaults/:class/:style/missing.jpg'
    :default_url => lambda { |cover| cover.instance.set_default_url}
    #:convert_options => {
    #  :all => '-blur 0x2'
   #}
  })
  validates_attachment :cover,
    :content_type => { :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  #  :size => { :in => 0..1700.kilobytes }

  def set_default_url
    ActionController::Base.helpers.asset_path('misc/broken-link.png')
  end

  # Relationships
  belongs_to :suggestion_type
  belongs_to :user #AUTHOR
  belongs_to :updated_by, :class_name => 'User'

  has_many :playlist, :through => :playlist_suggestion
  has_many :playlist_suggestion

  has_many :suggestion_users, :dependent => :destroy
  has_many :users, :through => :suggestion_users

  has_many :tags, :through => :suggestions_tags,
  :after_add => :update_index,
  :after_remove => :update_index

  has_many :suggestions_tags, :dependent => :destroy
  has_many :subproblems, :through => :suggestion_subproblems,
  :after_add => :update_index,
  :after_remove => :update_index
  has_many :suggestion_subproblems, :dependent => :destroy

  before_save :check_processor
  validate :check_lock, on: :update
  validate :suspend_public, on: [:create, :update]
  before_create :check_processor_new
  after_create :set_identifier
  before_save :update_lock_time

  accepts_nested_attributes_for :suggestion_users

  friendly_id :name, use: [:slugged, :finders]

  validates_presence_of :name
  validate :validate_image_url
  #validates_uniqueness_of :identifier

  scope :approved, -> { where(:approved => true) }

  # needs to have a user
  #elastis search indexs, mappings and searches
  def self.search_ahead_analysis
    {
      index: {
        number_of_shards: 1,
        number_of_replicas: 0,
        analysis: {
          filter: {
            autocomplete_filter: {
              type: 'edge_ngram',
              min_gram: 2,
              max_gram: 50
            },
            substring: {
              type: "nGram",
              min_gram: 1,
              max_gram: 50
            }
          },
          analyzer: {
            autocomplete: {
              type: 'custom',
              tokenizer: 'standard',
              filter: [
                'lowercase',
                'asciifolding',
                'autocomplete_filter'
              ]
            },
            str_search_analyzer: {
              tokenizer: "keyword",
              filter: ["lowercase"]
            },

            str_index_analyzer: {
              tokenizer: "keyword",
              filter: ["lowercase", "substring"]
            }
          }
        }
      }
    }
  end

  # elastic search mappings
  settings Suggestion.search_ahead_analysis do
    mappings dynamic: 'false' do
      indexes :name , :type => "string", store:"no", include_in_all: "false", index_analyzer: "autocomplete", search_analyzer: "standard"
      indexes :description , :type => "string", :type => "string", store:"no", include_in_all: "false", index_analyzer: "autocomplete", search_analyzer: "standard"
      indexes :approved, :type => "boolean"
      indexes :lc_cat, :type => "string"
      indexes :problems, :type => "string", :index =>"not_analyzed"
      indexes :problems_names, :type => "string", :index =>"not_analyzed"
      indexes :tags, :type => "string", :index =>"not_analyzed"
      indexes :users, :type => "string", :index =>"not_analyzed"
      indexes :url, :type => "string", store:"no", include_in_all: "false", index_analyzer: "str_index_analyzer", search_analyzer: "str_search_analyzer"
    end
  end

  #after_update -> {
  #  __elasticsearch__.index_document
  #}

  # update the lock time to be used in the check lock
  def update_lock_time
    self.lock_time = Time.now
  end

  def as_indexed_json(options={})
  {
    "name" => self.name,
    "description" => self.description,
    "approved" => self.approved,
    "lc_cat" => self.lifestyle_category,
    "problems" => self.subproblems.map(&:identifier),
    "problems_names" => self.subproblems.map(&:category),
    "tags" => self.tags.map(&:name),
    "users" => self.users.map(&:name),
    "url" => self.url,
  }
  end

  def self.search(query, approved = nil, size = 0, from =0)
    if query.is_a? String #for naked searches :)
      size =  Suggestion.count if size == 0
      if !approved.nil?
        Suggestion.__elasticsearch__.search(
            from: from,
            size: size,
            query: {
              multi_match: {
                query:query,
                fields: [ "name", "description", "lc_cat","users", "url" ],
                operator: "and"
              }
            },
             filter: {
                term: {approved: approved}
              }

         )
      else
        Suggestion.__elasticsearch__.search(
            from: from,
            size: size,
            query: {
              multi_match: {
                query:query,
                fields: [ "name", "description", "lc_cat","users", "url" ],
                operator: "and"
              }
            }
        )
      end
    else
      Suggestion.__elasticsearch__.search(query)
    end
  end


  def update_index(obj)
    self.__elasticsearch__.index_document
  end


  def completed_suggestions
    self.suggestion_users.where(:state => 2).count
  end

  def cover_from_url=(url_value)
    self.cover = URI.parse(url_value)
    # Assuming url_value is http://example.com/photos/face.png
    # avatar_file_name == "face.png"
    # avatar_content_type == "image/png"
    @cover_from_url = url_value
  end

  def self.deconstruct_url(url_string)
    logger.debug "URL: #{url_string}"
    res = WellclubPersistence::Helper.working_url?(url_string)

    if ( url_string  =~ /\Ahttp.*(jpeg|jpg|gif|png)\Z/ )
      out = res[0] ? res[1] : nil
    else
      out = nil
    end

    out
  end

  def self.deconstruct_difficulty(text)
    if (text == "Beginner" || text == "beginner")
      0
    elsif (text == "Capable" || text == "capable")
      1
    elsif (text == "Expert" || text == "expert")
      2
    else
      nil
    end
  end

  def check_lock
    locked = ((Time.now - self.lock_time) <= 5.0 ? true : false)
    errors.add(:base, "LOCK in effect - WAIT 5 seconds!") if locked
  end

  #def self.column_names
    #self.fields.collect { |field| field[0] }
    #["Criado em", "Actualizado Em", "UID", "Nome", "Email", "XP", "Last Code", "# Codes", "# Invites", "Sessions", "Check Primeira Festa?", "Check Segunda Festa?"]
  #end

  def self.export_csv
    (CSV.generate({:col_sep => ","}) do |csv|
      csv << ["Identifier", "Description", "Name", "Lifestyle Category", "Sub Problems", "Sub Sub Problems", "Likes/Dislikes",
      "Personal Ability", "Personal Motivation", "Social Ability", "Social Motivation", "Structural Ability", "Structural Motivation",
      "Difficulty", "Time Duration", "Actual Time", "Timeliness", "Restrictions", "Photo", "Video"]
      all.each do |sug|
        #csv << user.attributes.values_at(*column_names)

        cover = sug.cover.present? ? sug.cover.url.split("?").first : ""

        row = [
          sug.identifier,
          sug.description,
          sug.name,
          sug.lifestyle_cat.present? ? sug.lifestyle_cat : 'MISC',
          sug.subprob_cat,
          sug.subsubprob_cat,
          '', # Likes / Dislikes
          '',
          '',
          '',
          '',
          '',
          '',
          sug.difficult,
          1440,
          sug.duration,
          '', # Timeliness
          '', # Restrictions"
          cover,
          sug.url.present? ? sug.url.split("?").first : cover
        ]
        csv << row
      end
    end).encode(Encoding::UTF_8, :undef => :replace, :replace => '')
  end

  #Suggestion.importer("./db/seeding/suggestions_samples.csv")
  def self.import_csv(file, approved = true)

    ext = File.extname(file).gsub('.', '')
    xls = Roo::Spreadsheet.open(file, extension: ext.to_sym, encoding: Encoding::UTF_8)
    counter = 0

    sheet = xls.sheet(0)
    (1..sheet.last_row).each do |idx|
      row = sheet.row(idx)

      if idx == 1
        # first column validation
        return "Invalid CSV File format" unless (row[0] == "Identifier" && row[1] == "Description")
      else

        #["#", "Description","Title","Lifestyle Category (Eating, Movement, Self View)","Sub Problem Category","Sub-Sub Problem Category","Likes/Dislike ","Personal Ability","Personal Motivation","Social Ability","Social Motivation","Structural Ability","Structural Motivation","Difficulty","Time Duration","Actual time","Timeliness","Restriction?","Picture Link","Picture/Video"]

        #suggestion #;Suggestions;Title;Lifestyle Category (Eating, Movement, Self View);Sub Problem Category;Sub-Sub Problem Category;Likes/Dislike ;Personal Ability;Personal Motivation;Social Ability;Social Motivation;Structural Ability;Structural Motivation;Difficulty;Time Duration;Actual time;Timeliness;Restriction?;Picture Link;Picture/Video

        puts row[0]

        next if Suggestion.where(:identifier => row[0]).exists?

        counter += 1

        sug = Suggestion.create(
          :identifier => row[0], # suggestion id
          :description => row[1],
          :name => row[2],

          :lifestyle_category => LifestyleCategory::CATEGORIES.values.index(row[3]),

          :lifestyle_cat => row[3],
          :subprob_cat => row[4],
          :subsubprob_cat => row[5],

          # 6 likes and dislikes (tags)

          # 7, 8, 9 ,10, 11, 12  influencer

          :difficult => Suggestion.deconstruct_difficulty(row[13]),
          # 14 time duration <15 etc
          :duration => row[15],

          # 16 timeliness
          # 17 Restriction?

          :cover => Suggestion.deconstruct_url(row[18]),
          :url => (row[18].present? || row[19].present?) ? (row[19].present? ? row[19] : row[18]) : nil,

          :approved => approved.present? ? true : false
        )

        # Adding the problems to the suggestion
        problems = row[5].present? ? row[5].split(",") : []
        problems.each do |p_id|
          puts p_id
          problem = Subproblem.find_by(identifier: p_id.strip)

          next unless problem.present?

          sug.subproblems << problem if (!sug.subproblems.include?(problem))
        end

        tags = row[6].present? ? row[6].split(",") : []
        tags.each do |t_id|
          puts t_id
          tag = Tag.find_by(identifier: t_id.strip)

          next unless tag.present?

          sug.tags << tag if (!sug.tags.include?(tag))
        end

        res = sug.save

        puts sug.errors.messages unless res

      end
    end

    "SUGGESTIONS ADDED: #{counter}"
  end

  def validate_image_url
      if self.processor == "image" and Suggestion.detector(self.url) != :image and !self.cover.file?
        errors.add(:processor, "not valid image")
      end
  end

  def check_processor
    if self.url_changed?
      self.processor = Suggestion.detector(self.url)
    end
  end

  def suspend_public
    if self.public and not self.approved
      errors.add(:public, "Cannot suspend a public suggestion. A suggestion must be approved in order for it to be public.")
    end
  end

  def set_identifier
    unless self.identifier.present?
      life_cat = self.lifestyle_cat.present? ? self.lifestyle_cat : "MISC"

      candidate_identifier = "#{life_cat}#{self.id}"
      count = self.id
      while Suggestion.where(:identifier => candidate_identifier).exists? do
        count += 1
        candidate_identifier = "#{life_cat}#{count}"
      end
      # found new identifier
      self.identifier = candidate_identifier
      self.save
    end
  end

  def check_processor_new
    begin
    if self.cover.file? and self.url.blank?
      self.processor = :image
    elsif self.url.present?
      self.processor = Suggestion.detector(self.url)
      if self.processor == :image
        self.cover = URI.parse(self.url)
      end
    end
    rescue
    end
  end

  def outputter
    # first detects
    res = ''
    case self.processor
    when "image"
      # prints image link
      res = "<div class='image-content' style=\"background-image:url('#{self.cover(:mobile)}')\"></div>"
    when "youtube"
      # grab youtube id
      regex = /youtube.com.*(?:\/|v=)([^&$]+)/
      value = self.url.match(regex)
      value = self.url.match(/youtu.be.*(?:\/|v=)([^&$]+)/) if value.nil?
      if value.present?
        yt_id = value[1]
        res = "<div class='image-content'><iframe height='100%' src='//www.youtube.com/embed/#{yt_id}' frameborder='0' allowfullscreen></iframe></div>"
      end
    when "link"
      res = ActionController::Base.helpers.link_to(url,url, :target=>"_blank")
    else
      # no processor available for this
      res = ""
      res = "<div class='image-content' style=\"background-image:url('#{self.cover(:mobile)}')\"></div>" if self.cover.present?
    end

    res
  end

  # enum types of processing (image, youtube)
  def self.detector(url)

    if ( url  =~ /\Ahttp.*(jpeg|JPEG|jpg|JPG|gif|GIF|png|PNG)\Z/)
      # its an image
      :image
    elsif ( url =~ /^(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([\w-]{10,})/ )
      # its an youtube video, we need to grab an id
      :youtube
    else
      # we dont know or is a link
      :link
    end
  end

  def like_ratio
    likes = self.likes.present? ? self.likes : 0
    dislikes = self.dislikes.present? ? self.dislikes : 0

    likes > 0 || dislikes > 0 ? likes / (likes + dislikes) * 100 : 0
  end

  def matches_goal? goal
   (goal.subproblems & self.subproblems).present?
    #self.lifestyle_category == goal.lifestyle_category
  end


end
