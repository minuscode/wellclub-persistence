class SuggestionSubproblem < ActiveRecord::Base
  belongs_to :suggestion, touch: true
  belongs_to :subproblem
end
