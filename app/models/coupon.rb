class Coupon < ActiveRecord::Base
  has_many :payments

  before_save do
    self.code = self.code.upcase
  end
end
