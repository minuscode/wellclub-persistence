class SuggestionsTag < ActiveRecord::Base
  belongs_to :suggestion, touch: true
  belongs_to :tag
end
