class GoalUser < ActiveRecord::Base
  include AASM
  belongs_to :goal
  belongs_to :goal_task
  belongs_to :user

  has_attached_file :photo_motivation,
  {:styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => lambda { |logo| logo.instance.set_default_url}}.merge(Application::DEFAULTS)

  validates_attachment_content_type :photo_motivation, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  has_attached_file :photo_reward, {:styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => lambda { |logo| logo.instance.set_default_url} }.merge(Application::DEFAULTS)
  validates_attachment_content_type :photo_reward, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  # only for ios devices
  attr_accessor :orientation_reward, :orientation_motivation
  before_photo_motivation_post_process :verify_rotation_motivation
  before_photo_reward_post_process :verify_rotation_reward

  after_create :get_last_photos

  def get_last_photos
    if self.user.goal_users.where("status <> 'initialized' and status <> 'canceled'").present?
      last_goal = self.user.goal_users.where("status <> 'initialized' and status <> 'canceled'").last
      self.photo_motivation = last_goal.photo_motivation
      self.photo_reward = last_goal.photo_reward
      self.motivation_reason = last_goal.motivation_reason
      self.achieve_reward = last_goal.achieve_reward
      self.save
    end
  end

  def verify_rotation_motivation
    if self.orientation_motivation.present?
    #  path = (Rails.env.development? ? self.photo_motivation.path : self.photo_motivation.url)
      image = MiniMagick::Image.new(self.photo_motivation.queued_for_write[:original].path)
      if image.exif["Orientation"].present?
        if self.orientation_motivation.to_i == 6 || self.orientation_motivation.to_i == 3
          rotate = self.orientation_motivation.to_i == 6 ? "90" : "180"
          image.combine_options do |b|
            b.rotate rotate
          end
          self.orientation_motivation = ''
          self.photo_motivation = File.open(image.path)
        end
      end
    end
    extension = File.extname(photo_motivation_file_name).downcase
    self.photo_motivation.instance_write :file_name, "#{Time.now.to_i.to_s}#{extension}"
  end

  def verify_rotation_reward
    if self.orientation_reward.present?
    #  path = (Rails.env.development? ? self.photo_reward.path : self.photo_reward.url)
      image = MiniMagick::Image.new(self.photo_reward.queued_for_write[:original].path)
      if image.exif["Orientation"].present?
        if self.orientation_reward.to_i == 6 || self.orientation_reward.to_i == 3
          rotate = self.orientation_reward.to_i == 6 ? "90" : "180"
          image.combine_options do |b|
            b.rotate rotate
          end
          self.orientation_reward = ''
          self.photo_reward = File.open(image.path)
        end
      end
    end
    extension = File.extname(photo_reward_file_name).downcase
    self.photo_reward.instance_write :file_name, "#{Time.now.to_i.to_s}#{extension}"
  end

  def set_default_url
    ''
  end

  def update_progress(delta = 1)
    case self.goal.tracking_type
    when "suggestion"
      #Create a supportive environment for eating healthy 10 times in [2,3,4,5] weeks
      self.update_attribute(:complete_amount, self.complete_amount+delta)
    when "weight"
      self.update_attribute(:complete_amount, self.complete_amount+delta)
    else
      # we dont know what to do with it
    end

    default_amount = self.complete_amount.present? ? self.complete_amount : 10.0
    if default_amount >= self.goal_amount
      self.complete_goal
      self.end_date = Time.now
    end

    self.save
  end

  aasm :column => 'status' do
     state :initialized, :initial => true
     state :started
     state :completed
     state :canceled
     state :failed

     event :start_goal do
      transitions :from => [:initialized, :canceled], :to => :started
      after do
        PromptGoalWorker.perform_in(self.target_date+12.hours, self.id, :prompt_goal_expired)
      end
      error do |e|
      end
     end

     event :complete_goal do
      transitions :from => [:initialized, :started], :to => :completed
      after do
        send_notifications_to_supporters("prompt_goal_achieved")
      end
      error do |e|
      end
     end

     event :canceled_goal do
      transitions :from => [:initialized, :started], :to => :canceled
      after do
      end
      error do |e|
      end
     end

     event :failed_goal do
      transitions :from => :started, :to => :failed
      after do
        send_notifications_to_supporters("prompt_goal_expired")
      end
      error do |e|
      end
     end

  end

  def send_notifications_to_supporters(prompt)
      NotificationDispatcher.send_notification_goal_to_supporters(self, prompt)
  end

=begin

#POST MVP

  after_create :send_prompt_0

  def send_prompt_0
    self.send_prompt_0
  end

  aasm :column => 'prompt_status' do
     state :created, :initial => true
     #before 7 days goal expires
     state :prompt_goal_0
     #after goal expires
     state :prompt_goal_1
     #before 7 days goal expires
     state :prompt_goal_2
     #before 7 days goal expires
     state :prompt_goal_3
     #after finished goal
     state :prompt_goal_f

     event :send_prompt_0 do
      transitions :from => :created, :to => :prompt_goal_0
      after do
        if self.target_date > 7.days.from_now
          PromptWorker.perform_in(self.target_date - 7 days, self.id, self.user.name, :prompt_0)
        else
          self.send_prompt_1
        end
      end
      error do |e|
      end
     end

     event :send_prompt_1 do
      transitions :from => :prompt_goal_0, :to => :prompt_goal_1
      after do
        PromptWorker.perform_in(self.target_date, self.id, self.user.name, :prompt_goal_1) #prompt_0 18 + 6 = 24
      end
      error do |e|
      end
     end

     event :send_prompt_2 do
      transitions :from => :prompt_goal_1, :to => :prompt_goal_2
      after do
        PromptWorker.perform_in(12.hours, self.id, self.user.name, :prompt_goal_2) #prompt_0 18 + prompt_1 6 + 24 = 48
      end
      error do |e|
      end
     end

     event :send_prompt_3 do
       transitions :from => :prompt_goal_2, :to => :prompt_goal_3
       after do
        PromptWorker.perform_in(12.hours, self.id, self.user.name, :prompt_goal_3) #prompt_0 18 + prompt_1 6 + prompt_2 24 + 120  = 168
       end
       error do |e|
       end
     end

     event :send_prompt_f do
       transitions :from => [:created, :prompt_goal_0, :prompt_goal_1, :prompt_goal_2, :prompt_goal_3], :to => :prompt_goal_f
       after do
          send_emails_to_supporters("prompt_f", self.user.name, :prompt_goal_f)
       end
       error do |e|
       end

     end

  end

=end


end
