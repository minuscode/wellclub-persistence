class SuggestionUser < ActiveRecord::Base
  include AASM

  # Relationships
  belongs_to :user
  belongs_to :supporter, :class_name => 'User'
  belongs_to :suggestion
  belongs_to :parent_suggestion_user, :class_name => 'SuggestionUser'
  belongs_to :playlist
  has_many :suggestion_user_comments, :dependent => :destroy

  accepts_nested_attributes_for :suggestion_user_comments

  #Callbacks
  after_update :update_status_changed #for the after commit send notifications
  after_save :update_counter_cache
  after_commit :send_notifications, :on => :update
  before_destroy :destroy_notifications

  STATUS_MEMBER = %w[completed skipped did_not_complete]
  STATUS_SUPPORTER = %w[sent created]


=begin
  has_attached_file :photo, Application::SUGGESTION_PAPERCLIP_OPTS.merge({
    :styles => {
      :thumb   => 'x100',
      :mobile   => 'x400',
      :desktop => 'x600'
    },
    #:default_url => '/assets/defaults/:class/:style/missing.jpg'
    :default_url => lambda { |photo| photo.instance.set_default_url}
    #:convert_options => {
    #  :all => '-blur 0x2'
   #}
  })
  validates_attachment :photo,
    :content_type => { :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"] }


  # STATES
  # 1 STARTED
  # 2 COMPLETED
  # 3 FAILED
  # 4 SHARED
  # 5 DUPLICATE
  # 6 REJECTED
  # 7 DISMISSED
  # 8 RETRY

  aasm :column => 'state_status' do
     state :created, :initial => true
     state :completed
     state :failed
     state :shared

     event :complete_suggestion do
        transitions :from => [:created, :shared], :to => :completed
     end

     event :fail_suggestion do
        transitions :from => :created, :to => :completed
     end

     event :share_suggestion do
        transitions :from => :created, :to => :shared
     end

  end
=end
  #State machine
  aasm :column => 'prompt_status' do
     state :created, :initial => true
     #after 18h suggestion started
     state :prompt_0
     #after 24h suggestion started - expired suggestion
     state :prompt_1
     #after 48h suggestion started
     state :prompt_2
     #after 168h suggestion started
     state :prompt_3
     #after finished suggestion
     state :prompt_f

     event :send_prompt_0 do
      transitions :from => [:created, :prompt_0, :prompt_1, :prompt_2, :prompt_3, :prompt_f], :to => :prompt_0
      after do
        PromptWorker.perform_in(18.hours, self.id, self.user.name, :prompt_0)
      end
      error do |e|
      end
     end

     event :send_prompt_1 do
      transitions :from => :prompt_0, :to => :prompt_1
      after do
        PromptWorker.perform_in(6.hours, self.id, self.user.name, :prompt_1) #prompt_0 18 + 6 = 24
      end
      error do |e|
      end
     end

     event :send_prompt_2 do
      transitions :from => :prompt_1, :to => :prompt_2
      after do
        PromptWorker.perform_in(24.hours, self.id, self.user.name, :prompt_2) #prompt_0 18 + prompt_1 6 + 24 = 48
      end
      error do |e|
      end
     end

     event :send_prompt_3 do
       transitions :from => :prompt_2, :to => :prompt_3
       after do
        PromptWorker.perform_in(120.hours, self.id, self.user.name, :prompt_3) #prompt_0 18 + prompt_1 6 + prompt_2 24 + 120  = 168
       end
       error do |e|
       end
     end

     event :send_prompt_f do
       transitions :from => [:created, :prompt_0, :prompt_1, :prompt_2, :prompt_3, :prompt_f], :to => :prompt_f
       after do
       end
       error do |e|
       end

     end


  end

  #Methods
  def update_status_changed
    @state_has_been_changed = self.state_changed?
  end

  def send_notifications
      if @state_has_been_changed
        case self.state
          when 1 #started
            #instead of all supporters it will send to supporter who share the suggestion
            #self.send_emails_to_supporters('started_suggestion')
            send_notification("started_suggestion_supporter")
          when 2 #completed
            #its not working inside after on send_prompt_f state change
            send_notification_to_supporters("prompt_f")
          when 6 #rejected
            send_notification("rejected_suggestion_supporter")
          when 8 #retry
            send_notification_to_supporters("prompt_f")
        end
      end
  end

  def send_notification(prompt)
      NotificationDispatcher.send_notification_suggestion(self, prompt)
  end

  def send_notification_to_supporters(prompt)
      NotificationDispatcher.send_notification_suggestion_to_supporters(self, prompt)
  end

  def retrying_suggestion
    if self.state == 8 #if its retrying
      NotificationDispatcher.send_notification_suggestion(self, "retried_suggestion_supporter")
    end
  end

  def destroy_notifications
    UserNotification.joins(:notification).where("notifications.class_type = 2 and table_id = #{self.id}").destroy_all
  end

  def start_existing_suggestion

    self.state = 1
    self.like = nil
    self.start_date = Time.now

    self.send_prompt_0
    self.save

    # mark all other suggestion user for this suggestion as duplicates
    SuggestionUser.where(:state => 4, :suggestion_id => self.suggestion_id, :user_id => self.user_id).update_all(:state => 5)
  end

  def complete_suggestion
    self.state = 2

    # check if suggestion counts toward a goal
    #raise "xx"
    if self.user.has_active_goal?
      active_goal = self.user.active_goal
      if active_goal.goal.tracking_type == "suggestion" && suggestion.lifestyle_category.present? && active_goal.goal.lifestyle_category.present?
          active_goal.update_progress(1) if suggestion.lifestyle_category.eql? active_goal.goal.lifestyle_category
      end
    end

    self.end_date = Time.now
    self.send_prompt_f!
    self.save
  end

  def fail_suggestion
    self.state = 3
    self.end_date = Time.now
    self.send_prompt_f!
    self.save
  end

  def share_suggestion
    self.state = 4
    self.save
  end

  def skip_suggestion
    self.state = 7
    self.save
  end

  def deny_suggestion
    self.state = 6
    self.end_date = Time.now
    self.save
  end

  def retry_suggestion
    self.state = 8
    self.end_date = Time.now
    self.save
  end

  def reset_suggestion
    self.start_date = Time.now
    self.save
  end

  def to_retry?
    self.state == 8
  end


  def update_like(value = true)
    self.like = value
    self.save
  end

  def get_state
    case self.state
       when 1
          "Accepted"
       when 2
          "Completed"
       when 3
          "Didn't completed"
       when 4
          "Shared"
       #duplicate shared
       when 5
          "Shared"
       when 6
          "Rejected"
       when 7
          "\"Maybe later\""
       when 8
          "Retried"
    end
  end


  def self.share_to_all(admin_id, suggestion_ids)
    SuggestionUser.share(admin_id, User.member_users.map(&:id), suggestion_ids)
  end

  def self.share(admin_id, user_ids, suggestion_ids)
    user = User.find(admin_id)
    user_ids.each do |user_id|
      suggestion_ids.each do |suggestion_id|
          SuggestionUser.create_shared_suggestion(Suggestion.find(suggestion_id), User.find(user_id), user)
      end
    end
  end

  def self.create_complete_suggestion(suggestion, user, supporter, comment = '')
    suggestion_user = SuggestionUser.new
    suggestion_user.user = user
    suggestion_user.suggestion = suggestion
    suggestion_user.complete_suggestion
  end

  def self.create_start_suggestion(suggestion, user, supporter, comment = '')
    suggestion_user = SuggestionUser.new
    suggestion_user.user = user
    suggestion_user.suggestion = suggestion
    suggestion_user.start_existing_suggestion
  end

  def self.create_shared_suggestion(suggestion, user, supporter, comment = '')

    parent_suggestion = SuggestionUser.where(:state => 4, :suggestion => suggestion, :user => user, :parent_suggestion_user => nil).order(:created_at)
    already_shared_by_the_user = SuggestionUser.where(:state => 4, :suggestion => suggestion, :user => user, :supporter => supporter)

    suggestion_user = ((already_shared_by_the_user.blank?) ? SuggestionUser.new : already_shared_by_the_user.first)
    suggestion_user.user = user
    suggestion_user.supporter = supporter
    suggestion_user.suggestion = suggestion
    suggestion_user.parent_suggestion_user = parent_suggestion.first if parent_suggestion.present? and parent_suggestion != suggestion_user
    suggestion_user.share_suggestion
    suggestion_user_to_use = (parent_suggestion.present? ? parent_suggestion.first : suggestion_user)
    SuggestionUserComment.create_comment(comment, supporter, suggestion_user_to_use, true) if comment.present?

    suggestion_user.send_notification("shared_suggestion")
    #UserMailer.supporter_share(suggestion_user).deliver if suggestion_user.id.present?
  end

  #the default rails counter cache don't have conditions, it counts all records
  def update_counter_cache

    connection = ActiveRecord::Base.connection

    if self.state == 2 and self.state_changed?
      connection.execute("UPDATE suggestions SET completed = (coalesce(completed, 0) + 1) WHERE id = #{self.suggestion.id}")
      connection.execute("UPDATE users SET suggestions_completed = (coalesce(suggestions_completed, 0) + 1) WHERE id = #{self.user.id}")
      self.suggestion.touch
    end

    if self.state == 3 and self.state_changed?
      connection.execute("UPDATE users SET suggestions_failed = (coalesce(suggestions_failed, 0) + 1) WHERE id = #{self.user.id}")
    end

    if self.state == 7 and self.state_changed?
      connection.execute("UPDATE users SET suggestions_dismissed = (coalesce(suggestions_dismissed, 0) + 1) WHERE id = #{self.user.id}")
    end

    if self.like_changed? and !self.like.nil?
      if self.like
        connection.execute("UPDATE suggestions SET likes =  (coalesce(likes, 0) + 1) WHERE id = #{self.suggestion.id}")
      else
        connection.execute("UPDATE suggestions SET dislikes =  (coalesce(dislikes, 0) + 1) WHERE id = #{self.suggestion.id}")
      end
      self.suggestion.touch
    end

  end



end
